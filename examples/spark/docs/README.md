## pyspark
- Installation Manual: https://spark.apache.org/docs/latest/api/python/getting_started/install.html
- docker container
```sh
docker run -it --rm spark:python3 /opt/spark/bin/pyspark
```
- Quickstart: Pandas API on Spark: https://spark.apache.org/docs/latest/api/python/getting_started/quickstart_ps.html
- Quickstart: DataFrame : https://spark.apache.org/docs/latest/api/python/getting_started/quickstart_df.html 
