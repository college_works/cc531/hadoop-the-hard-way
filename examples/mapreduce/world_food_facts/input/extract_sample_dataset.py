#!/usr/bin/env python3

import argparse
import pandas as pd


if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Extract sample dataset of large one")
    parser.add_argument("dataset")
    parser.add_argument("-s", "--samples", type=int, help="Number of random samples to take") 
    parser.add_argument("-o", "--output", default="output.txt", help="Output file")

    args = parser.parse_args()

    import pdb;pdb.set_trace()
    dataset = pd.read_csv(args.dataset)
    sample = dataset.sample(n=args.samples, random_state=1)
    sample.to_csv(args.output)
