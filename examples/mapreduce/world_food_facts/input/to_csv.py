#!/usr/bin/env python3

import os
import sys
import pandas as pd

if len(sys.argv) < 1:
    print(f"USAGE: {sys.argv[0]} <DATASET>")
    exit(1)

tsv_file=sys.argv[1]
csv_table=pd.read_table(tsv_file,sep='\t')
csv_table.to_csv(f"{os.path.splitext(tsv_file)[0]}.csv",index=False)
