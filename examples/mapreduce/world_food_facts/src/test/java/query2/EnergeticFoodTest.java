//package query2;

/*
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;

import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;

// assert
import static org.assertj.core.api.Assertions.assertThat;


public class EnergeticFoodTest {
  private MapDriver<LongWritable, Text, Text, FloatWritable> mapDriver;
  private ReduceDriver<Text, FloatWritable, Text, FloatWritable> reduceDriver;
  private MapReduceDriver<LongWritable, Text, Text, FloatWritable, Text, FloatWritable> mapReduceDriver;

  @BeforeClass
  public void setup(){
    mapDriver = MapDriver.newMapDriver(new EnergeticFoodMapper());
    reduceDriver = ReduceDriver.newReduceDriver(new EnergeticFoodReducer());
  }

  @Test
  public void CSVHeaderEntryMapperTest(){
    String csvHeader = "code,url,creator,created_t,created_datetime,last_modified_t,last_modified_datetime,product_name,generic_name,quantity,packaging,packaging_tags,brands,brands_tags,categories,categories_tags,categories_en,origins,origins_tags,manufacturing_places,manufacturing_places_tags,labels,labels_tags,labels_en,emb_codes,emb_codes_tags,first_packaging_code_geo,cities,cities_tags,purchase_places,stores,countries,countries_tags,countries_en,ingredients_text,allergens,allergens_en,traces,traces_tags,traces_en,serving_size,no_nutriments,additives_n,additives,additives_tags,additives_en,ingredients_from_palm_oil_n,ingredients_from_palm_oil,ingredients_from_palm_oil_tags,ingredients_that_may_be_from_palm_oil_n,ingredients_that_may_be_from_palm_oil,ingredients_that_may_be_from_palm_oil_tags,nutrition_grade_uk,nutrition_grade_fr,pnns_groups_1,pnns_groups_2,states,states_tags,states_en,main_category,main_category_en,image_url,image_small_url,energy_100g,energy-from-fat_100g,fat_100g,saturated-fat_100g,-butyric-acid_100g,-caproic-acid_100g,-caprylic-acid_100g,-capric-acid_100g,-lauric-acid_100g,-myristic-acid_100g,-palmitic-acid_100g,-stearic-acid_100g,-arachidic-acid_100g,-behenic-acid_100g,-lignoceric-acid_100g,-cerotic-acid_100g,-montanic-acid_100g,-melissic-acid_100g,monounsaturated-fat_100g,polyunsaturated-fat_100g,omega-3-fat_100g,-alpha-linolenic-acid_100g,-eicosapentaenoic-acid_100g,-docosahexaenoic-acid_100g,omega-6-fat_100g,-linoleic-acid_100g,-arachidonic-acid_100g,-gamma-linolenic-acid_100g,-dihomo-gamma-linolenic-acid_100g,omega-9-fat_100g,-oleic-acid_100g,-elaidic-acid_100g,-gondoic-acid_100g,-mead-acid_100g,-erucic-acid_100g,-nervonic-acid_100g,trans-fat_100g,cholesterol_100g,carbohydrates_100g,sugars_100g,-sucrose_100g,-glucose_100g,-fructose_100g,-lactose_100g,-maltose_100g,-maltodextrins_100g,starch_100g,polyols_100g,fiber_100g,proteins_100g,casein_100g,serum-proteins_100g,nucleotides_100g,salt_100g,sodium_100g,alcohol_100g,vitamin-a_100g,beta-carotene_100g,vitamin-d_100g,vitamin-e_100g,vitamin-k_100g,vitamin-c_100g,vitamin-b1_100g,vitamin-b2_100g,vitamin-pp_100g,vitamin-b6_100g,vitamin-b9_100g,folates_100g,vitamin-b12_100g,biotin_100g,pantothenic-acid_100g,silica_100g,bicarbonate_100g,potassium_100g,chloride_100g,calcium_100g,phosphorus_100g,iron_100g,magnesium_100g,zinc_100g,copper_100g,manganese_100g,fluoride_100g,selenium_100g,chromium_100g,molybdenum_100g,iodine_100g,caffeine_100g,taurine_100g,ph_100g,fruits-vegetables-nuts_100g,fruits-vegetables-nuts-estimate_100g,collagen-meat-protein-ratio_100g,cocoa_100g,chlorophyl_100g,carbon-footprint_100g,nutrition-score-fr_100g,nutrition-score-uk_100g,glycemic-index_100g,water-hardness_100g";

    mapDriver.withInput(new LongWritable(), new Text(csvHeader));
    
    List<Pair<Text, FloatWritable>> results = mapDriver.run();

    assertThat(result).
      hasSize(0);
  }

  @Test 
  public void noEnergyValueMapperTest(){
    // record has no energy value
    String invalidRecord_b64 = "MCwzMDg3LGh0dHA6Ly93b3JsZC1lbi5vcGVuZm9vZGZhY3RzLm9yZy9wcm9kdWN0LzAwMDAwMDAwMDMwODcvZmFyaW5lLWRlLWJsZS1ub2lyLWZlcm1lLXQteS1yLW5hbyxvcGVuZm9vZGZhY3RzLWNvbnRyaWJ1dG9ycywxNDc0MTAzODY2LDIwMTYtMDktMTdUMDk6MTc6NDZaLDE0NzQxMDM4OTMsMjAxNi0wOS0xN1QwOToxODoxM1osRmFyaW5lIGRlIGJsw6kgbm9pciwsMWtnLCwsRmVybWUgdCd5IFInbmFvLGZlcm1lLXQteS1yLW5hbywsLCwsLCwsLCwsLCwsLCwsLGVuOkZSLGVuOmZyYW5jZSxGcmFuY2UsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCJlbjp0by1iZS1jb21wbGV0ZWQsIGVuOm51dHJpdGlvbi1mYWN0cy10by1iZS1jb21wbGV0ZWQsIGVuOmluZ3JlZGllbnRzLXRvLWJlLWNvbXBsZXRlZCwgZW46ZXhwaXJhdGlvbi1kYXRlLXRvLWJlLWNvbXBsZXRlZCwgZW46Y2hhcmFjdGVyaXN0aWNzLXRvLWJlLWNvbXBsZXRlZCwgZW46Y2F0ZWdvcmllcy10by1iZS1jb21wbGV0ZWQsIGVuOmJyYW5kcy1jb21wbGV0ZWQsIGVuOnBhY2thZ2luZy10by1iZS1jb21wbGV0ZWQsIGVuOnF1YW50aXR5LWNvbXBsZXRlZCwgZW46cHJvZHVjdC1uYW1lLWNvbXBsZXRlZCwgZW46cGhvdG9zLXRvLWJlLXZhbGlkYXRlZCwgZW46cGhvdG9zLXVwbG9hZGVkIiwiZW46dG8tYmUtY29tcGxldGVkLGVuOm51dHJpdGlvbi1mYWN0cy10by1iZS1jb21wbGV0ZWQsZW46aW5ncmVkaWVudHMtdG8tYmUtY29tcGxldGVkLGVuOmV4cGlyYXRpb24tZGF0ZS10by1iZS1jb21wbGV0ZWQsZW46Y2hhcmFjdGVyaXN0aWNzLXRvLWJlLWNvbXBsZXRlZCxlbjpjYXRlZ29yaWVzLXRvLWJlLWNvbXBsZXRlZCxlbjpicmFuZHMtY29tcGxldGVkLGVuOnBhY2thZ2luZy10by1iZS1jb21wbGV0ZWQsZW46cXVhbnRpdHktY29tcGxldGVkLGVuOnByb2R1Y3QtbmFtZS1jb21wbGV0ZWQsZW46cGhvdG9zLXRvLWJlLXZhbGlkYXRlZCxlbjpwaG90b3MtdXBsb2FkZWQiLCJUbyBiZSBjb21wbGV0ZWQsTnV0cml0aW9uIGZhY3RzIHRvIGJlIGNvbXBsZXRlZCxJbmdyZWRpZW50cyB0byBiZSBjb21wbGV0ZWQsRXhwaXJhdGlvbiBkYXRlIHRvIGJlIGNvbXBsZXRlZCxDaGFyYWN0ZXJpc3RpY3MgdG8gYmUgY29tcGxldGVkLENhdGVnb3JpZXMgdG8gYmUgY29tcGxldGVkLEJyYW5kcyBjb21wbGV0ZWQsUGFja2FnaW5nIHRvIGJlIGNvbXBsZXRlZCxRdWFudGl0eSBjb21wbGV0ZWQsUHJvZHVjdCBuYW1lIGNvbXBsZXRlZCxQaG90b3MgdG8gYmUgdmFsaWRhdGVkLFBob3RvcyB1cGxvYWRlZCIsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLAo="


  }
  @Test
  public void alwaysTrue(){
    assert true;
  }
}
*/
