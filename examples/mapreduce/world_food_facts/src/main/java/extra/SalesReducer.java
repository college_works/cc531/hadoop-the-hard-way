package extra;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import java.lang.Math;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.UUID;


import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class SalesReducer extends SalesCombiner {
  private final String SPLITTER = "_";

  private final UUID uuid = UUID.randomUUID();
  private Logger logger = Logger.getLogger(SalesReducer.class + "." + uuid.toString());

  
  @Override
  public Iterable<AccumulatorWritable> reducer(Iterable<AccumulatorWritable> values){    
    UUID reducerUUID = UUID.randomUUID(); // nota parece que la clase reducer se esta reusando (ASI QUE agregaremos un identificador por reducer)

    Logger reducerLogger = Logger.getLogger(logger.getName() + "." + reducerUUID.toString());

    Iterable<AccumulatorWritable> globalValues = super.reducer(values);

    float countryTotalSales = 0.0f;
    // state
    Map<String, AccumulatorWritable> stateTotalSales = new HashMap<>();

    String key;
    AccumulatorWritable accumulator;

    // computing country total sales and states total sales
    for(AccumulatorWritable value : globalValues){
      reducerLogger.info("[country=" + this.country + ", globalAccumulator=" + value.toString() + "]");
      countryTotalSales += value.getSales();
      reducerLogger.info("[country=" + this.country + ", countryTotalSales=" + String.valueOf(countryTotalSales) + "]");

      key = value.getState();
      try {
        AccumulatorWritable valueState = value.copy();
        valueState.setCity("");
      if (stateTotalSales.containsKey(key)){
        accumulator = stateTotalSales.get(key);
        accumulator.increment(valueState);
        stateTotalSales.replace(key, accumulator.copy());
      } else {
        // set value.city == NULL
        stateTotalSales.put(key, valueState);
      }
      } catch(CloneNotSupportedException error){
        reducerLogger.warn(error.toString());
      }

      reducerLogger.info("[stateTotalSales = " + stateTotalSales + "]");
    }

    float threshold = countryTotalSales / 4.0f;
    reducerLogger.info("threshold = " + String.valueOf(threshold));
    // AcumulatorWritable(state=STATE, city=NULL, sales=SALES)
    List<AccumulatorWritable> filtered = new ArrayList<>();


    // cities that have sales greater than CountrySales / 4
    for(AccumulatorWritable cityAccumulator : globalValues){
      if(cityAccumulator.getSales() > threshold){
        cityAccumulator.setThreshold(threshold);
        filtered.add(cityAccumulator);
      }
    }

    reducerLogger.info("[CITIES] filtered=" + filtered); 
    // <S1, C1> NO
    // <S1, C2> NO
    // ==========
    // <S1, C1> SI AccumulatorWritable(state=S1, city=C1, sales=SALES)
    // <S1, C2> NO

    // states that have sales greates than CountrySales / 4
    for(AccumulatorWritable stateAccumulator : stateTotalSales.values()){
      if(stateAccumulator.getSales() > threshold){
        stateAccumulator.setThreshold(threshold);
        filtered.add(stateAccumulator);
      }
    }

    reducerLogger.info("[STATES] filtered=" + filtered);
    // S1 SI
    // =====
    // S1 SI AccumultorWritable(state=S1, city=null, sales=SALES)
    
    return filtered;
  }

  @Override
  public void writeOutput(Text key, Iterable<AccumulatorWritable> results, Context context)  throws IOException, InterruptedException
{
  for(AccumulatorWritable result : results){
    logger.info("ReducerOuput(<key= " + key.toString() +", value=" + result.toString() + ">)");
    context.write(key, result);
  }
  }

}
