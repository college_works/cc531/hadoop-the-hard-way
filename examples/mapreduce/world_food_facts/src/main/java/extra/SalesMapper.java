package extra;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import org.apache.commons.lang3.StringUtils;

// csv utilities
import org.apache.commons.csv.CSVFormat; // CSV parser
import org.apache.commons.csv.CSVRecord;
import java.io.StringReader;

import org.apache.commons.lang3.StringUtils;
import java.util.UUID;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;


public class SalesMapper extends Mapper<LongWritable, Text, Text, AccumulatorWritable> {
  private final static String COUNTRY_CSV_HEADER = "Country";
  private final UUID uuid = UUID.randomUUID();
  private Logger logger = Logger.getLogger(SalesMapper.class + "." + uuid.toString());
  private final static CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                                              .setSkipHeaderRecord(true)
                                              .build();
  enum InvalidValues {
    NULL_PRODUCT_ENERGY,
    NOT_NUMERIC_ENERGY
  }
 
  @Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		StringReader rowCSVDataset = new StringReader(value.toString()); 
   
    Iterable<CSVRecord> records = csvFormat.parse(rowCSVDataset); // parse csv row with library
    for(CSVRecord record : records){
      // check input/DataExploratory.ipynb for more details about field indexes
      String country = StringUtils.strip(record.get(7)); // Country
      String state = StringUtils.strip(record.get(6)); // State
                                    
      String city = StringUtils.strip(record.get(5)); // City
      String priceText = StringUtils.strip(record.get(2)); // Price
                                     

      try {
        if (country != null &&
            !country.equals(COUNTRY_CSV_HEADER) && 
            !country.isEmpty()){


          float price = Float.parseFloat(priceText);
          // float with ','
          // cities with extra space (remove space)
          AccumulatorWritable x = new AccumulatorWritable(state, city, price);

          logger.info("MapperOutput(<key= " + country +", value=" + x.toString() + ">)");
          context.write(new Text(country), x);
        }
      } catch(NumberFormatException error){
        logger.warn(error.toString());
        //context.getCounter(InvalidValues.NOT_NUMERIC_ENERGY).increment(1);

      } catch(NullPointerException error){
        logger.warn(error.toString());
        //context.getCounter(InvalidValues.NULL_PRODUCT_ENERGY).increment(1);
      }
    }
	}
}
