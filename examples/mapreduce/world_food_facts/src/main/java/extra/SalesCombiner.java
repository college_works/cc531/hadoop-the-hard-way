package extra;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library

import java.util.UUID;
import java.util.HashMap;
import java.util.Map;
//import javafx.util.Pair;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class SalesCombiner extends Reducer<Text, AccumulatorWritable, Text, AccumulatorWritable> {
  private final String SPLITTER = "_";

  private final UUID uuid = UUID.randomUUID();
  private Logger logger = Logger.getLogger(SalesCombiner.class + "." + uuid.toString());

  protected String country;

  public Iterable<AccumulatorWritable> reducer(Iterable<AccumulatorWritable> values){
    UUID reducerUUID = UUID.randomUUID(); // nota parece que la clase reducer se esta reusando (ASI QUE agregaremos un identificador por reducer)

    Logger reducerLogger = Logger.getLogger(logger.getName() + "." + reducerUUID.toString());

    // state_city
    Map<String, AccumulatorWritable> mapAccumulator = new HashMap<String, AccumulatorWritable>();

    String key;
    AccumulatorWritable accumulator;


    int index =0;
    for(AccumulatorWritable value : values){

      reducerLogger.info("LOOP_MAP" + String.valueOf(index) + 
          " [mapAccumulator=" + mapAccumulator + ", hashcode=" + mapAccumulator.hashCode() + "]");


      reducerLogger.info("[LOOP " + String.valueOf(index) + 
          " [country=" + country+ ", value = " + value.toString() + ", hashCode=" + value.hashCode() + "]");
      key = value.getState() + SPLITTER + value.getCity();
      try{
      if (mapAccumulator.containsKey(key)){
        AccumulatorWritable partialAccumulator = mapAccumulator.get(key);

        reducerLogger.info("LOOP-IF " + String.valueOf(index) + " [country=" + country+ ", value = " + partialAccumulator.toString() + "]");
        partialAccumulator.increment(value);


        reducerLogger.info("LOOP-IF_INCREMENT " + String.valueOf(index) + " [country=" + country+ ", value = " + partialAccumulator.toString() + "]");

        mapAccumulator.replace(key, partialAccumulator.copy());

      } else {
        mapAccumulator.put(key, value.copy());
      }

      } catch(CloneNotSupportedException error){
        reducerLogger.warn(error.toString());
      }


      reducerLogger.info("LOOP" + String.valueOf(index) + 
          " [mapAccumulator=" + mapAccumulator + ", hashcode=" + mapAccumulator.hashCode() + "]");

      reducerLogger.info("LOOP_UPDATE " + String.valueOf(index) + " [value=" + mapAccumulator.get(key).toString() + "]");
      index = index + 1;
    }

    return mapAccumulator.values();
  }
  
  @Override
  public void reduce(Text key, Iterable<AccumulatorWritable> values, Context context) throws IOException, InterruptedException
  {   
    this.country = key.toString(); 
    Iterable<AccumulatorWritable> results = reducer(values);
    writeOutput(key, results, context);  
  }

  public void writeOutput(Text key, Iterable<AccumulatorWritable> results, Context context)  throws IOException, InterruptedException
{
  for(AccumulatorWritable result : results){
    logger.info("CombinerOutput(<key= " + key.toString() +", value=" + result.toString() + ">)");
    context.write(key, result);
  }
  }
}
