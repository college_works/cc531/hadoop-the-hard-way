package extra;

import java.io.IOException;

import java.lang.Cloneable;

import java.io.DataOutput;
import java.io.DataInput;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.FloatWritable;


public class AccumulatorWritable implements Writable, Cloneable  {
  private String state;
  private String city;
  private float sales;
  private float threshold;

  public AccumulatorWritable() {}

  public AccumulatorWritable(String state, String city, float sales) {
    this.state = state;
    this.city = city;
    this.sales = sales;
    this.threshold = 0.0f;
  }

  public void increment(AccumulatorWritable accumulator){
    if(state.equals(accumulator.getState()) & city.equals(accumulator.getCity())){
      sales += accumulator.getSales();
    }
  }
  
  public void readFields(DataInput in) throws IOException {
    state = in.readUTF();
    city = in.readUTF();
    sales = in.readFloat();
  }

  public void write(DataOutput out) throws IOException {
    out.writeUTF(state);
    out.writeUTF(city);
    out.writeFloat(sales);
  }

  public AccumulatorWritable copy() throws CloneNotSupportedException {
    return (AccumulatorWritable) this.clone();
  }

  public void setCity(String city){ this.city = city; }
  public void setThreshold(float threshold){ this.threshold = threshold; }

  @Override
  protected Object clone() throws CloneNotSupportedException {
        return super.clone();
  }



  //public void set(float value){ super.set(value); }
  public float getSales(){ return sales; }
  public String getState(){ return state; }
  public String getCity(){ return city; }

  /*
  public void multiply(float value){ 
    this.set(this.get()*value);
    elements = elements +1;
  }
  */

  /*
  public void multiply(ProductWritable product){
    this.set(this.get()*product.get());
    elements = elements + product.getElements();
  }
  */

  //@Override
  public String toString(){
    String out = "Accumulator(state=" + state + ", city=" + city +
      ", sales=" + String.valueOf(sales) + 
      ", threshold=" + String.valueOf(threshold) +")";

    return out;
  }
}
