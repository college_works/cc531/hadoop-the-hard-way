package query2;

import java.io.IOException;
import java.lang.InterruptedException;

import java.util.Iterator;
import org.apache.log4j.Logger; // logging library

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
//import org.apache.hadoop.mapreduce.Reducer.Context;

public class EnergeticFoodReducer extends  Reducer<Text, FloatWritable, Text, FloatWritable> {

  private Logger logger = Logger.getLogger(EnergeticFoodReducer.class); 

  @Override
  public void reduce(Text t_key, Iterable<FloatWritable> values, Context context) throws IOException, InterruptedException {
		Text brand = t_key;

		float minEnergy = Float.MAX_VALUE;
    float maxEnergy = Float.MIN_VALUE;

    for(FloatWritable value : values){ 
      logger.info("ReduceInput [key=" + brand.toString() + ", energy=" + value.toString() + "]");
      float energy = value.get();

      if (Float.compare(minEnergy, energy) > 0){ minEnergy = energy; }
      if (Float.compare(maxEnergy, energy) < 0){ maxEnergy = energy; }	
		}

		context.write(brand, new FloatWritable(minEnergy));
		context.write(brand, new FloatWritable(maxEnergy));
	}
}
