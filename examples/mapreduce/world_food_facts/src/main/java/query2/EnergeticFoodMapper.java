package query2;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library

// csv utilities
import org.apache.commons.csv.CSVFormat; // CSV parser
import org.apache.commons.csv.CSVRecord;
import java.io.StringReader;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;



public class EnergeticFoodMapper extends Mapper<LongWritable, Text, Text, FloatWritable> {
  private final static String BRANDS_CSV_HEADER = "brands";
  private Logger logger = Logger.getLogger(EnergeticFoodMapper.class);
  private final static CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                                              .setSkipHeaderRecord(true)
                                              .build();
  enum InvalidEnergyValues {
    NULL,
    NOT_NUMERIC
  }

  @Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		StringReader rowCSVDataset = new StringReader(value.toString()); // brands: 12, energy_100g 63 (check input/DataExploratory.ipynb file)
    
    Iterable<CSVRecord> records = csvFormat.parse(rowCSVDataset); // parse csv row with library
    for(CSVRecord record : records){
      String brand = record.get(12); // brand
      String energy = record.get(63); // energy_100g

      if (!brand.equals(BRANDS_CSV_HEADER) && !brand.isEmpty()){
        try {
          float numericEnergy = Float.parseFloat(energy);

          logger.info("Writing <" + brand +", " + energy + ">"); // onyl for debug purpose [DELETE IT]
          context.write(new Text(brand), new FloatWritable(numericEnergy));

        } catch(NumberFormatException error){
          context.getCounter(InvalidEnergyValues.NOT_NUMERIC).increment(1);
          logger.warn("ERROR: " + error.toString());

        } catch(NullPointerException error) {
          context.getCounter(InvalidEnergyValues.NULL).increment(1);
          logger.warn("ERROR: " + error.toString());
        }
      }
    }
	}
}
