package query3;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import java.lang.System;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class RegisteredProductsReducer extends Reducer<Text, DateWritable, Text, DateWritable> {
  private Logger logger = Logger.getLogger(RegisteredProductsReducer.class);
  public LocalDateTime startDateTime;
  public LocalDateTime endDateTime;

  public RegisteredProductsReducer(){
    String START_DATETIME = System.getenv("START_DATETIME");
    if(START_DATETIME.isEmpty()){
      startDateTime = LocalDateTime.parse(START_DATETIME, DateTimeFormatter.ISO_INSTANT);
    } else {
     startDateTime = LocalDateTime.MIN; 
    }


    String END_DATETIME = System.getenv("END_DATETIME");
    if(END_DATETIME.isEmpty()){
      endDateTime = LocalDateTime.parse(END_DATETIME, DateTimeFormatter.ISO_INSTANT);
    } else {
     endDateTime = LocalDateTime.MIN; 
    }
  }

  public boolean isBetween(LocalDateTime start, LocalDateTime end, DateWritable target) {
    LocalDateTime targetDateTime = target.getDateTime();
    
    return start.isAfter(targetDateTime) & end.isBefore(targetDateTime);
  } 
 
  @Override
  public void reduce(Text key, Iterable<DateWritable> values, Context context) throws IOException, InterruptedException
  {
    logger.info("START_DATETIME=" + startDateTime.toString() + ", END_DATETIME=" + endDateTime.toString());

    for(DateWritable date : values){
      if(isBetween(startDateTime, endDateTime, date)){
        logger.info("ReducerOutput(<key=" + key.toString() + ", value=" + date.toString() + ">)");
        context.write(key, date);
      }
    }
  }
}
