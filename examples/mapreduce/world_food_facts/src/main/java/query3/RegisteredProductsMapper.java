package query3;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import org.apache.commons.lang3.StringUtils;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

// csv utilities
import org.apache.commons.csv.CSVFormat; // CSV parser
import org.apache.commons.csv.CSVRecord;
import java.io.StringReader;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;


public class RegisteredProductsMapper extends Mapper<LongWritable, Text, Text, DateWritable> {
  private final static String PRODUCT_NAME_CSV_HEADER = "product_name";
  private Logger logger = Logger.getLogger(RegisteredProductsMapper.class);
  private final static CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                                              .setSkipHeaderRecord(true)
                                              .build();

  enum InvalidValues {
    INVALID_CREATED_DATETIME
  }
 
  @Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		StringReader rowCSVDataset = new StringReader(value.toString()); 
   
    Iterable<CSVRecord> records = csvFormat.parse(rowCSVDataset); // parse csv row with library
    for(CSVRecord record : records){
      // check input/DataExploratory.ipynb for more details about field indexes
      String productName = record.get(7); // product_name
      String datetimeText = record.get(4); // created_datetime 

      try {
        if (productName != null &&
            !productName.equals(PRODUCT_NAME_CSV_HEADER) && 
            !productName.isEmpty()){

          DateWritable date = new DateWritable(datetimeText);

          logger.info("MapperOuput(<key= " + productName +", value=" + date.toString() + ">)");
          context.write(new Text(productName), date);
        }
      } catch(DateTimeParseException error){
        logger.warn(error.toString());
        context.getCounter(InvalidValues.INVALID_CREATED_DATETIME).increment(1);
      }
    }
	}
}
