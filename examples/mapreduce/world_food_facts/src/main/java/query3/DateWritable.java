package query3;

import java.io.IOException;

import java.io.DataOutput;
import java.io.DataInput;
import org.apache.hadoop.io.Writable;

import java.lang.CharSequence;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateWritable implements Writable {
  private LocalDateTime datetime;
  private static final String pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
  private static final DateTimeFormatter datasetFormatter = DateTimeFormatter.ofPattern(pattern);


  public DateWritable() {}
  public DateWritable(CharSequence text, DateTimeFormatter formatter){
    datetime = LocalDateTime.parse(text, formatter);
  }

  public DateWritable(CharSequence text){
    datetime = LocalDateTime.parse(text, datasetFormatter);
  }

  public LocalDateTime getDateTime(){ return datetime; }

  public void readFields(DataInput in) throws IOException {
    int year = in.readInt();
    int month = in.readInt();
    int dayOfMonth = in.readInt();
    int hour = in.readInt();
    int minute = in.readInt();
    int second = in.readInt();

    datetime = LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
  }

  public void write(DataOutput out) throws IOException {
    out.writeInt(datetime.getYear());
    out.writeInt(datetime.getMonthValue());
    out.writeInt(datetime.getDayOfMonth());
    out.writeInt(datetime.getHour());
    out.writeInt(datetime.getMinute());
    out.writeInt(datetime.getSecond());
  }

  @Override
  public String toString(){
    String out = "DateWritable(date=" + datetime.toString() + ")"; 

    return out;
  }
}
