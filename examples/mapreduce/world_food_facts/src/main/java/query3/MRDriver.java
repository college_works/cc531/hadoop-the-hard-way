package query3;

import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MRDriver {
  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();

    Job job = Job.getInstance(conf, "RegisteredProducts");
    job.setJarByClass(MRDriver.class);
    job.setMapperClass(RegisteredProductsMapper.class);
    job.setCombinerClass(RegisteredProductsReducer.class);
    job.setReducerClass(RegisteredProductsReducer.class);
 
   
    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(DateWritable.class);
    // job.
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(DateWritable.class);

    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));

    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
