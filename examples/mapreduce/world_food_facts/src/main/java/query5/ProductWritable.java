package query5;

import java.io.IOException;

import java.io.DataOutput;
import java.io.DataInput;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.FloatWritable;


public class ProductWritable extends FloatWritable {
  private int elements; // number of multiplied values

  public ProductWritable() {
    super(1.0f);
    elements = 0;
  }

  public ProductWritable(float value) {
    super(value);
    elements = 1;
  }


  public void readFields(DataInput in) throws IOException {
    super.readFields(in);
    elements = in.readInt();
  }

  public void write(DataOutput out) throws IOException {
    super.write(out);
    out.writeInt(elements);
  }

  //public void set(float value){ super.set(value); }
  public int getElements(){ return elements; }

  public void multiply(float value){ 
    this.set(this.get()*value);
    elements = elements +1;
  }

  public void multiply(ProductWritable product){
    this.set(this.get()*product.get());
    elements = elements + product.getElements();
  }

  @Override
  public String toString(){
    String out = "ProductWritable(result=" + 
      String.valueOf(this.get()) + ", elements=" + String.valueOf(elements) + ")";

    return out;
  }
}
