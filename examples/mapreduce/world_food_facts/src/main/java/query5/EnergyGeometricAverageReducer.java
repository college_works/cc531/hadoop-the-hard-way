package query5;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import java.lang.Math;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class EnergyGeometricAverageReducer extends EnergyGeometricAverageCombiner {
  private Logger logger = Logger.getLogger(EnergyGeometricAverageReducer.class);
  
  @Override
  public ProductWritable reducer(Iterable<ProductWritable> values){    
    ProductWritable globalProduct = new ProductWritable();

    for(ProductWritable partialProduct : values){ 
      globalProduct.multiply(partialProduct);
    }

    float result = (float) Math.pow(globalProduct.get(), 1.0/globalProduct.getElements());
    globalProduct.set(result);

    return globalProduct;
  }

  @Override
  public void writeOutput(Text key, ProductWritable result, Context context)  throws IOException, InterruptedException
{
    logger.info("ReducerOuput(<key= " + key.toString() +", value=" + result.toString() + ">)");
    context.write(key, result);
  }

}
