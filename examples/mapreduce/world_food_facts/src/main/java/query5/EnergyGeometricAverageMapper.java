package query5;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import org.apache.commons.lang3.StringUtils;

// csv utilities
import org.apache.commons.csv.CSVFormat; // CSV parser
import org.apache.commons.csv.CSVRecord;
import java.io.StringReader;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;


public class EnergyGeometricAverageMapper extends Mapper<LongWritable, Text, Text, ProductWritable> {
  private final static String BRANDS_TAGS_CSV_HEADER = "brands_tags";
  private Logger logger = Logger.getLogger(EnergyGeometricAverageMapper.class);
  private final static CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                                              .setSkipHeaderRecord(true)
                                              .build();
  enum InvalidValues {
    NULL_PRODUCT_ENERGY,
    NOT_NUMERIC_ENERGY
  }
 
  @Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		StringReader rowCSVDataset = new StringReader(value.toString()); 
   
    Iterable<CSVRecord> records = csvFormat.parse(rowCSVDataset); // parse csv row with library
    for(CSVRecord record : records){
      // check input/DataExploratory.ipynb for more details about field indexes
      String brand_tag = record.get(13); // brands_tags
      String energy = record.get(63); // energy_100g
                                        

      try {
        if (brand_tag != null &&
            !brand_tag.equals(BRANDS_TAGS_CSV_HEADER) && 
            !brand_tag.isEmpty()){

          float numericEnergy = Float.parseFloat(energy);

          logger.info("MapperOuput(<key= " + brand_tag +", value=" + energy + ">)");
          context.write(new Text(brand_tag), new ProductWritable(numericEnergy));
        }
      } catch(NumberFormatException error){
        logger.warn(error.toString());
        context.getCounter(InvalidValues.NOT_NUMERIC_ENERGY).increment(1);

      } catch(NullPointerException error){
        logger.warn(error.toString());
        context.getCounter(InvalidValues.NULL_PRODUCT_ENERGY).increment(1);
      }
    }
	}
}
