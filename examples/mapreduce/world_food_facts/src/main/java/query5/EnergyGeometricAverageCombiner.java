package query5;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library

import java.util.UUID;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class EnergyGeometricAverageCombiner extends Reducer<Text, ProductWritable, Text, ProductWritable> {
  private final UUID uuid = UUID.randomUUID();
  private Logger logger = Logger.getLogger(EnergyGeometricAverageCombiner.class + "." + uuid.toString());

  public ProductWritable reducer(Iterable<ProductWritable> values){
    ProductWritable product = new ProductWritable();
    for(ProductWritable value : values){
      product.multiply(value);
    }

    return product;
  }
  
  @Override
  public void reduce(Text key, Iterable<ProductWritable> values, Context context) throws IOException, InterruptedException
  {    
    ProductWritable result = reducer(values);
    writeOutput(key, result, context);  
  }

  public void writeOutput(Text key, ProductWritable result, Context context)  throws IOException, InterruptedException
{
    logger.info("CombinerOuput(<key= " + key.toString() +", value=" + result.toString() + ">)");
    context.write(key, result);
  }
}
