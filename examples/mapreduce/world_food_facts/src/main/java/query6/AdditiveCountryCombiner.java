package query6;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import java.util.Map;
import java.util.HashMap;

import java.util.UUID;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class AdditiveCountryCombiner extends Reducer<Text, AdditiveCounterWritable, Text, AdditiveCounterWritable> {
  private final UUID uuid = UUID.randomUUID();
  private Logger logger = Logger.getLogger(AdditiveCountryCombiner.class + "." + uuid.toString());
  protected Text key;

  public Iterable<AdditiveCounterWritable> reducer(Iterable<AdditiveCounterWritable> values){
    Map<String, AdditiveCounterWritable> counterMap = new HashMap<String, AdditiveCounterWritable>();
    AdditiveCounterWritable counter;


 
    for(AdditiveCounterWritable additiveCounter : values){
      String additive = additiveCounter.getAdditive();
      logger.info("LOOP[ additive=" + additive + ", counter=" + String.valueOf(additiveCounter.getCounter()) + "]");

      if(counterMap.containsKey(additive)){
        counter = counterMap.get(additive);
        counter.count(additiveCounter);
        counterMap.replace(additive, counter);
      } else {
        counterMap.put(additive, additiveCounter);
      }

      counter = counterMap.get(additive);
      logger.info("[key=" + this.key + 
          ", additive=" + additive + 
          ", counter=" + String.valueOf(counter.getCounter()) + "]");
    }

    for(Map.Entry<String, AdditiveCounterWritable> entry : counterMap.entrySet()){
      logger.info("Results_entrySet [ additive=" + entry.getKey() + ", counter=" + entry.getValue().toString() + "]");
    }

    for(AdditiveCounterWritable x : counterMap.values()){
      logger.info("Results_values [ additive=" + x.getAdditive() + ", counter=" + x.toString() + "]");
    }

    return counterMap.values();
  }
  
  @Override
  public void reduce(Text key, Iterable<AdditiveCounterWritable> values, Context context) throws IOException, InterruptedException
  {    
    this.key = key;
    Iterable<AdditiveCounterWritable> results = reducer(values);
    writeOutput(key, results, context);
  }

  public void writeOutput(Text key, Iterable<AdditiveCounterWritable> results, Context context)  throws IOException, InterruptedException {
    for(AdditiveCounterWritable additiveCounter : results){
      logger.info("CombinerOutput(<key= " + key.toString() +", value=" + additiveCounter.toString() + ">)");
      context.write(key, additiveCounter);
    }
  }
}
