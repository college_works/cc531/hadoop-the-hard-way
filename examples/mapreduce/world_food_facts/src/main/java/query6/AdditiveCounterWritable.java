package query6;

import java.io.IOException;

import java.io.DataOutput;
import java.io.DataInput;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.Text;


public class AdditiveCounterWritable implements Writable {
  private String additive; // additives_tags
  int counter; // number of products that registered in a country (key pair) that has the additive

  public AdditiveCounterWritable() { }

  public AdditiveCounterWritable(String additive){
    this.additive = additive;
    counter = 1;
  }

  public void readFields(DataInput in) throws IOException {
    additive = in.readUTF();
    counter = in.readInt();
  }

  public void write(DataOutput out) throws IOException {
    out.writeUTF(additive);
    out.writeInt(counter);
  }

  public void count(AdditiveCounterWritable additiveCounter){
    if(additive.equals(additiveCounter.getAdditive())){
      counter += additiveCounter.getCounter();
    }
  }

  public void setCounter(int value) { counter = value; }
  public int getCounter(){ return counter; }
  public String getAdditive(){ return additive; }

  @Override
  public String toString(){
    String out = "AdditiveCounterWritable(additive=" + additive + 
      ", counter=" + String.valueOf(counter) + ")";

    return out;
  }
}
