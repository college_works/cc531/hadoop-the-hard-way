package query6;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

import java.util.UUID;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class AdditiveCountryReducer extends AdditiveCountryCombiner {

  private final UUID uuid = UUID.randomUUID();
  private Logger logger = Logger.getLogger(AdditiveCountryReducer.class + "." + uuid.toString());

  @Override
  public Iterable<AdditiveCounterWritable> reducer(Iterable<AdditiveCounterWritable> values){
    Iterable<AdditiveCounterWritable> globalCounters = super.reducer(values);

    AdditiveCounterWritable frequentAdditive = new AdditiveCounterWritable("NONE");
    frequentAdditive.setCounter(Integer.MIN_VALUE);

    for(AdditiveCounterWritable additiveCounter : globalCounters){
      logger.info("GlobalCounter(key=" + this.key + ", value=" + additiveCounter.getCounter() + ")");
      if(frequentAdditive.getCounter() < additiveCounter.getCounter()){
        frequentAdditive = additiveCounter;
      }
    }

    ArrayList<AdditiveCounterWritable> result = new ArrayList<AdditiveCounterWritable>();
    result.add(frequentAdditive);

    return result;
  }
  
  @Override
  public void writeOutput(Text key, Iterable<AdditiveCounterWritable> results, Context context)  throws IOException, InterruptedException
  {
    for(AdditiveCounterWritable frequentAdditive : results){ // one iteration
      logger.info("ReducerOuput(<key= " + key.toString() +", value=" + frequentAdditive.toString() + ">)");
      context.write(key, frequentAdditive);
    }
  }
}
