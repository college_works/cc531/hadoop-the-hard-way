package query6;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import org.apache.commons.lang3.StringUtils;

// csv utilities
import org.apache.commons.csv.CSVFormat; // CSV parser
import org.apache.commons.csv.CSVRecord;
import java.io.StringReader;

import java.util.UUID;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

public class AdditiveCountryMapper extends Mapper<LongWritable, Text, Text, AdditiveCounterWritable> {
  private final static String COUNTRIES_TAGS_CSV_HEADER = "countries_tags";
  private Logger logger = Logger.getLogger(AdditiveCountryMapper.class);
  private final static CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                                              .setSkipHeaderRecord(true)
                                              .build();

  enum InvalidValues {
    NULL_ADDITIVES
  }
 
  @Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		StringReader rowCSVDataset = new StringReader(value.toString()); 
   
    Iterable<CSVRecord> records = csvFormat.parse(rowCSVDataset); // parse csv row with library
    for(CSVRecord record : records){
      String country = record.get(32); // countries_tags
      String additivesText = record.get(44); // additives_tags
                                        

      if (!country.equals(COUNTRIES_TAGS_CSV_HEADER) && 
          !country.isEmpty()){

        String[] additivies = additivesText.split(",");

        for(String additive : additivies){
          logger.info("[country=" + country + ", product_name=" + record.get(7) + ", additive=" + additive + "]");
          if (!additive.isEmpty()){
            AdditiveCounterWritable additiveCounter = new AdditiveCounterWritable(additive);
            logger.info("MapperOutput(<key= " + country +", value=" + additiveCounter.toString() + ">)");
            context.write(new Text(country), additiveCounter);
          } else {
            context.getCounter(InvalidValues.NULL_ADDITIVES).increment(1);
          }
        }
      }
    }
	}
}
