package query4;

import java.io.IOException;
import java.lang.InterruptedException;

import java.util.Iterator;
import org.apache.log4j.Logger; // logging library

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


/*
 * Since the Mapper didn't perform some class of filter on entries.
 *
 * This class (Reducer) is going to perform them following the next criteria:
 *  - Valid products must have values (no empty or NULL) for the following fields [proteins_100g, ingredients_text] (Check input/DataExploratory for more details)
 *  - proteins_100g is a numeric field
 *  - quantity field can be NaN
 */
public class OrganicFoodReducer extends  Reducer<Text, FoodWritable, Text, FoodWritable> {

  private Logger logger = Logger.getLogger(OrganicFoodReducer.class); 

  enum InvalidValues { // used for counting invalid values
    NOT_NUMERIC_PROTEINS,
    NULL_PRODUCT_PROTEINS,
    NULL_PRODUCT_INGREDIENTS,
    NULL_PRODUCT_QUANTITY
  }

  @Override
  public void reduce(Text key, Iterable<FoodWritable> values, Context context) throws IOException, InterruptedException {
		Text product_name = key;

    for(FoodWritable food : values){ 
      logger.info("ReducerInput(<key=" + key.toString() + ", value=" + food.toString() + ">)");

      try {
        float proteins = Float.parseFloat(food.getProteins());
        String ingredients = food.getIngredients();
        String quantity = food.getQuantity();

        if((ingredients != null && !ingredients.isEmpty()) && 
            (quantity != null)){ // quantity can have NaN values
          logger.info("ReducerOutput<key=" + key.toString() + ", value=" + food.toString() + ">)");
          context.write(key, food);

        } else if ( ingredients == null) {
          context.getCounter(InvalidValues.NULL_PRODUCT_INGREDIENTS).increment(1);
          logger.warn("Product " + food + " has no ingredients");

        } else {
          context.getCounter(InvalidValues.NULL_PRODUCT_QUANTITY).increment(1);
          logger.warn("Product " + food.toString() + "has no quantity");
        }

      } catch (NumberFormatException error) {
        context.getCounter(InvalidValues.NOT_NUMERIC_PROTEINS).increment(1);
        logger.warn(error.toString());

      }	catch (NullPointerException error) {
        context.getCounter(InvalidValues.NULL_PRODUCT_PROTEINS).increment(1);
        logger.warn(error.toString());
      }
    }
	}
}
