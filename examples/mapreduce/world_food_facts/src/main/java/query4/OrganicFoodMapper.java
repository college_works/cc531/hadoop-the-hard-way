package query4;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import org.apache.commons.lang3.StringUtils;

// csv utilities
import org.apache.commons.csv.CSVFormat; // CSV parser
import org.apache.commons.csv.CSVRecord;
import java.io.StringReader;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;


/*
 * Some values of proteins_100g, ingredients_text and quantity are NaN.
 * But because some NaN value are significant for some situation (inmesurable quantities). 
 * All the records are passed to Reduce for a post-analysis and to maintain the SRP (Single Responsability Principle)
 * of the Mapper.
 *
 * That os why we force the proteins_100g field on FoodWritable data type to be String instead of float (data type of dataset).
 */
public class OrganicFoodMapper extends Mapper<LongWritable, Text, Text, FoodWritable> {
  private final static String PRODUCT_NAME_CSV_HEADER = "product_name";
  private Logger logger = Logger.getLogger(OrganicFoodMapper.class);
  private final static CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                                              .setSkipHeaderRecord(true)
                                              .build();
 
  @Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		StringReader rowCSVDataset = new StringReader(value.toString()); 
   
    Iterable<CSVRecord> records = csvFormat.parse(rowCSVDataset); // parse csv row with library
    for(CSVRecord record : records){
      // check input/DataExploratory.ipynb for more details about field indexes
      String product_name = record.get(7); // product_name
      String proteins = record.get(112); // proteins_100g
      String ingredients = record.get(34); // ingredients_text
      String quantity = record.get(9); // quantity
                                        

      if (!product_name.equals(PRODUCT_NAME_CSV_HEADER) && 
          !product_name.isEmpty() &&
          StringUtils.containsIgnoreCase(product_name, "Organic")){
        FoodWritable food = new FoodWritable(proteins, ingredients, quantity);

        logger.info("MapperOuput(<key= " + product_name +", value=" + food.toString() + ">)");
        context.write(new Text(product_name), food);
      }
    }

    // This case is for validate the serialization and deserialization of FoodWritable data type.
    //context.write(new Text("special_mapper_output"), new FoodWritable( Float.parseFloat("100.0"), null, null));
	}
}
