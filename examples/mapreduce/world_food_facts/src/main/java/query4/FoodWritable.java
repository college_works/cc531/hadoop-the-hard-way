package query4;

import java.io.IOException;

import java.io.DataOutput;
import java.io.DataInput;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.Text;


public class FoodWritable implements Writable {
  private String proteins; // data type: float (some values are nan - passed to Reduce for post analysis)
  private String ingredients; // data type: String
  private String quantity; // data type: String (some values are nan - passed to Reduce for post analysis)

  public FoodWritable() { }

  public FoodWritable(String proteins, String ingredients, String quantity){
    this.proteins = proteins;
    this.ingredients = ingredients;
    this.quantity = quantity;
  }

  public void readFields(DataInput in) throws IOException {
    proteins = in.readUTF();
    ingredients = in.readUTF();
    quantity = in.readUTF();
  }

  public void write(DataOutput out) throws IOException {
    out.writeUTF(proteins);
    out.writeUTF(ingredients);
    out.writeUTF(quantity);
  }

  public String getQuantity() { return quantity; }
  public String getIngredients(){ return ingredients; }
  public String getProteins(){ return proteins; }

  @Override
  public String toString(){
    String out = "FoodWritable(proteins=" +
      proteins + ", ingredients=" + ingredients +
      ", quantity=" + quantity + ")";

    return out;
  }
}
