package query1;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.FloatWritable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class EnergeticFoodReducer extends MapReduceBase
        implements Reducer<Text, ProductComponentsWritable, Text, ProductComponentsWritable> {

    public void reduce(Text t_key, Iterator<ProductComponentsWritable> values, OutputCollector<Text, ProductComponentsWritable> output, Reporter reporter) throws IOException {
        Text label = t_key;
        int frequency = 0;

        float carbohydrateSum = 0.0f;
        float proteinsSum = 0.0f;
        float fatSum = 0.0f;

        while (values.hasNext()) {
            // replace type of value with the actual type of our value
            ProductComponentsWritable components = values.next();
            carbohydrateSum += components.getCarbohydrates();
            proteinsSum += components.getProteins(); 
            fatSum += components.getFat(); 
            frequency += 1;
        }

        ProductComponentsWritable result = new ProductComponentsWritable(carbohydrateSum/frequency,
            proteinsSum/frequency,
            fatSum/frequency);
                                                                            

        output.collect(label, result);
    }
}
