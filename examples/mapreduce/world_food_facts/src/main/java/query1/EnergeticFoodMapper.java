package query1;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import org.apache.log4j.Logger; // logging library

// csv utilities
import org.apache.commons.csv.CSVFormat; // CSV parser
import org.apache.commons.csv.CSVRecord;
import java.io.StringReader;


import org.apache.hadoop.io.FloatWritable;

public class EnergeticFoodMapper extends MapReduceBase
        implements Mapper<LongWritable, Text, Text, ProductComponentsWritable> {

      private Logger logger = Logger.getLogger(EnergeticFoodMapper.class);
    
    private final float maxCarbohydrates = 50;
    private final float minProteins = 70;
    private final float maxFat = 60;
    private final String PRDUCT_NAME_HEADER = "product_name";
    private final static CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                                              .setSkipHeaderRecord(true)
                                              .build();


    public void map(LongWritable key, Text value, OutputCollector<Text, ProductComponentsWritable> output, Reporter reporter) throws IOException {

    StringReader rowCSVDataset = new StringReader(value.toString());      
      
    Iterable<CSVRecord> records = csvFormat.parse(rowCSVDataset); // parse csv row with library
    for(CSVRecord record : records){
        String productName = record.get(7); // product_name
        String carbohydrateText = record.get(101); // carbohudrate_100g
        String proteinText = record.get(112); // proteins_100g
        String fatText = record.get(65); // fat_100g
          
     
        try {

          logger.info("carbohydrate=" + carbohydrateText + ", proteine=" + proteinText + ", fat=" + fatText);


          float carbohydrate = Float.parseFloat(carbohydrateText);
          float proteine = Float.parseFloat(proteinText);
          float fat = Float.parseFloat(fatText);



        boolean isNotFirstRow = !productName.equals(PRDUCT_NAME_HEADER);  
        boolean isCarbohydrateLower = carbohydrate < maxCarbohydrates;
        boolean isProteinsGreater = proteine > minProteins;
        boolean isFatLower = fat < maxFat;


        if (isNotFirstRow && 
            isCarbohydrateLower &&
            isProteinsGreater &&
            isFatLower) {   

                   ProductComponentsWritable components = new ProductComponentsWritable(carbohydrate, proteine, fat);
            
            String label = productName;
            logger.info("MapperOutput(<key="+label + ", value=" + components.toString() + ">)");
            output.collect(new Text(label), components);
        }

        } catch (NullPointerException | NumberFormatException  error){
          logger.warn(error.toString());
        }
      }
    }
}
