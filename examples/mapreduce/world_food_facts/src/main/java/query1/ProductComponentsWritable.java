package query1;

import java.io.IOException;

import java.io.DataOutput;
import java.io.DataInput;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.Text;


public class ProductComponentsWritable implements Writable {
  private float proteins; 
  private float carbohydrates; 
  private float fat; 

  public ProductComponentsWritable() { }

  public ProductComponentsWritable(float proteins, float carbohydrates, float fat){
    this.proteins = proteins;
    this.carbohydrates = carbohydrates;
    this.fat = fat;
  }

  public void readFields(DataInput in) throws IOException {
    proteins = in.readFloat();
    carbohydrates = in.readFloat();
    fat = in.readFloat();
  }

  public void write(DataOutput out) throws IOException {
    out.writeFloat(proteins);
    out.writeFloat(carbohydrates);
    out.writeFloat(fat);
  }

  public float getProteins() { return proteins; }
  public float getCarbohydrates(){ return carbohydrates; }
  public float getFat(){ return fat; }

  @Override
  public String toString(){
    String out = "ProductComponentsWritable(proteins=" +
      String.valueOf(proteins) + ", carbohydrates=" + String.valueOf(carbohydrates) +
      ", fat=" + String.valueOf(fat) + ")";

    return out;
  }
}
