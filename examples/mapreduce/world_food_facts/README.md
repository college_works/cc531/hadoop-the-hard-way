
`Dataset`: [Open Food Fact - Kaggle](https://www.kaggle.com/datasets/openfoodfacts/world-food-facts)
> NOTE: A sample of the dataset can be found on [input](./input), there you can find the following utilities:
>  - Scripts to manipulate the dataset (tcv -> csv, ...)
>  - Jupyter Notebook that perform a data exploratory of the sample dataset to help you develop MapReduce programs and validate them.

## Queries

> NOTE: When a query has `OUTPUT_VALUE` == **RESULT**, then check the operation description.

1. a

2. What are the most and least energetic foods of each brand? [DONE]
    - Mapper(output=[**KEY** = "${brands\_tags}", **VALUE** = ${energy\_100g}])
        - class: `Mapper<LongWritable, Text, Text, FloatWritable>`
        - operation: Parse CSV dataset and extract pairs <`${brands_tags}`, `${energy_100g}`>

    - Combiner == Reducer

    - Reducer(output=[**KEY** = "${brands_tags}", **VALUE** = *RESULT*])
        - class: `Reducer<Text, FloatWritable, Text, FloatWritable>`
        - operation: Compute max and min values of passed data (`Iterable<FloatWritable>`) and generate <`${brands_tags}`, MIN_VALUE> and <`${brands_tags}`, MAX_VALUE>

3. What products were registered in the last month?

4. Look for organic products [DONE]
    - Mapper

5. Compute the energy geometric average of a product for each brand. [DONE]
    - Mapper(input=product\_name, output=[**KEY**="${brands\_tags}/${product\_name}", **VALUE**=${energy\_100g}])
        - operation: SOME DESCRIPTION OF THE OPERATION THAT THIS OPERATOR DOES
    - a

6. Where products that exceed the average fat content are best sold?
    - a 

## Structure
```
|- gradle.build (gradle configuration file to package source code and run tests)
|- input (data for queries)
|- src/main
    |- java
        |- query4
            |- MRDriver.java (maprecude entry point - job definition)
            |- other_dot_java_files (mapper, reducer, helper functions)
        ...
    |- test
        |- query4
            |- test-for-mapreduce-for-query4
        ...
```

## Quick Start

* Build code
```sh
    # check the generated jar on build/lib/
    ./gradlew clean jar
```

* Load dataset to hdfs
```sh
hdfs dfs -mkdir /input/world_food_facts
hdfs dfs -copyFromLocal input/en.openfoodfacts.org.products_25.csv /input/world_food_facts
```

* Run a query
```sh
    # NOTE: Similar for other queries
    # hadoop jar GENERATED_JAR [JAVA_PACKAGE].query4.MRDriver /path/to/input /output/query1

    hadoop jar build/libs/world_food_facts-4.0.jar query4.MRDriver /input/world_food_facts/en.openfoodfacts.org.products_25.csv /output/world_food_facts/query4/0001


    # If query has subqueries
    hadoop GENERATED_JAR [JAVA_PACKAGE].query1.<SUBQUERY>.MRDriver /path/to/input /output/query1/<SUBQUERY>
```
* Check job output
```sh
    hdfs dfs -cat /path/to/query/output/
```

* Check logs of job
```sh
    # get application id from output of map reduce job submition
    # patterns to search on log: [ MapperOutput, ReducerInput, ReducerOutput, FoodMapper, FoodReducer ]
    yarn logs -applicationId application_1694820806916_0013
```

