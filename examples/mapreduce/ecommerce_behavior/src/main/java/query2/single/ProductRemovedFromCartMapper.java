package query2.single;

import query2.CustomDataWritable;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class ProductRemovedFromCartMapper extends MapReduceBase
        implements Mapper<LongWritable, Text, Text, CustomDataWritable> {

    private final String HEADER_EVENT_TYPE = "event_type";
    private final String targetBrand = "baden";
    private final String targetEventType = "view";
    private final String targetCategoryCode = "shoes";

    public boolean isMatch(String regex, String text) {
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        return matcher.find();
    }

    public void map(LongWritable key, Text value,
            OutputCollector<Text, CustomDataWritable> output,
            Reporter reporter) throws IOException {

        String valueString = value.toString();
        String[] rowCSVDataset = valueString.split(",");

        // Columns
        String eventType = rowCSVDataset[1];
        String categoryCode = rowCSVDataset[4];
        String brand = rowCSVDataset[5];
        String priceText = rowCSVDataset[6];

        // Conditions
        boolean isNotEmpty = !eventType.isEmpty()
                && !categoryCode.isEmpty()
                && !brand.isEmpty()
                && !priceText.isEmpty();

        boolean isNotFirstRow = !eventType.equals(HEADER_EVENT_TYPE);

        if (isNotEmpty && isNotFirstRow) {
            boolean isTargetEventType = eventType.equals(targetEventType);
            boolean isTargetBrand = brand.equals(targetBrand);
            boolean isTargetCategoryCode = isMatch(targetCategoryCode,
                    categoryCode);
             
            if (isTargetEventType && isTargetBrand && isTargetCategoryCode) {
                float price = Float.parseFloat(priceText);
                
                output.collect(
                        new Text(eventType),
                        new CustomDataWritable(brand, categoryCode, price));
            }
        }
    }

}
