package query2.single;

import query2.CustomDataWritable;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.FloatWritable;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class ProductRemovedFromCartReducer extends MapReduceBase
        implements Reducer<Text, CustomDataWritable, Text, CustomDataWritable> {

    @Override
    public void reduce(Text key, Iterator<CustomDataWritable> values,
            OutputCollector<Text, CustomDataWritable> output, Reporter reporter)
            throws IOException {

        CustomDataWritable customDataWritable;
        Text brandWritable;
        Text categoryCodeWritable;
        FloatWritable priceWritable;

        String brand = "";
        String categoryCode = "";
        float price = 0;
        float maxPrice = -1;

        while (values.hasNext()) {

            customDataWritable = values.next();
            priceWritable = customDataWritable.getPrice();
            price = priceWritable.get();

            if (price > maxPrice) {
                brandWritable = customDataWritable.getBrand();
                categoryCodeWritable = customDataWritable.getCategoryCode();

                brand = brandWritable.toString();
                categoryCode = categoryCodeWritable.toString();
                maxPrice = price;
            }

        }
        output.collect(key, new CustomDataWritable(brand,
                categoryCode,
                maxPrice));
    }
}
