package parcialquery2mul;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class ProductRemovedFromCartReducer extends MapReduceBase
        implements Reducer<Text, IntWritable, Text, IntWritable> {

    @Override
    public void reduce(Text key, Iterator<IntWritable> values,
            OutputCollector<Text, IntWritable> output, Reporter reporter)
            throws IOException {

        
        int frequency = 0;
        IntWritable value;
        while (values.hasNext()) {
            // replace type of value with the actual type of our value
             value = values.next();
            frequency += value.get();

        }
        
        output.collect(key, new IntWritable(frequency));

    }
}

class ProductRemovedFromCartReducer2 extends MapReduceBase
        implements Reducer<Text, IntWritable, Text, IntWritable> {

    @Override
    public void reduce(Text key, Iterator<IntWritable> values,
            OutputCollector<Text, IntWritable> output, Reporter reporter)
            throws IOException {

        
        int frequency = 0;
        IntWritable value;
        while (values.hasNext()) {
            // replace type of value with the actual type of our value
             value = values.next();
            frequency += value.get();

        }
        
        output.collect(key, new IntWritable(frequency));

    }
}

class ProductRemovedFromCartReducer3 extends MapReduceBase
        implements Reducer<Text, CustomDataWritable, Text, CustomDataWritable> {

    @Override
    public void reduce(Text key, Iterator<CustomDataWritable> values,
            OutputCollector<Text, CustomDataWritable> output, Reporter reporter)
            throws IOException {

        CustomDataWritable customDataWritable;
        Text brandWritable;
        Text categoryCodeWritable;
        FloatWritable priceWritable;

        String brand = "";
        String categoryCode = "";
        float price = 0;
        float maxPrice = -1;

        while (values.hasNext()) {

            customDataWritable = values.next();
            priceWritable = customDataWritable.getPrice();
            price = priceWritable.get();

            if (price > maxPrice) {
                brandWritable = customDataWritable.getBrand();
                categoryCodeWritable = customDataWritable.getCategoryCode();

                brand = brandWritable.toString();
                categoryCode = categoryCodeWritable.toString();
                maxPrice = price;
            }

        }
        output.collect(key, new CustomDataWritable(brand,
                categoryCode,
                maxPrice));
    }
}
