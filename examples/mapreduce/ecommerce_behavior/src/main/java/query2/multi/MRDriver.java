package parcialquery2mul;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;


public class MRDriver {

    public static void main(String[] args) {
        
        // MAPREDUCER 1      
        JobClient my_client = new JobClient();
        JobConf job_conf1 = new JobConf(MRDriver.class);
        job_conf1.setJobName("MAPREDUCER 1");
        job_conf1.setOutputKeyClass(Text.class);
        job_conf1.setOutputValueClass(IntWritable.class);
        job_conf1.setMapperClass(ProductRemovedFromCartMapper.class);
        job_conf1.setReducerClass(ProductRemovedFromCartReducer.class);
        job_conf1.setInputFormat(TextInputFormat.class);
        job_conf1.setOutputFormat(TextOutputFormat.class);

        FileInputFormat.setInputPaths(job_conf1, new Path(args[0]));
        FileOutputFormat.setOutputPath(job_conf1, new Path(args[1]));
        my_client.setConf(job_conf1);
        //#########################################

        // MAPREDUCER 2
        JobClient my_client2 = new JobClient();
        JobConf job_conf2 = new JobConf(MRDriver.class);
        job_conf2.setJobName("MAPREDUCER 2");
        job_conf2.setOutputKeyClass(Text.class);
        job_conf2.setOutputValueClass(IntWritable.class);
        job_conf2.setMapperClass(ProductRemovedFromCartMapper2.class);
        job_conf2.setReducerClass(ProductRemovedFromCartReducer2.class);
        job_conf2.setInputFormat(TextInputFormat.class);
        job_conf2.setOutputFormat(TextOutputFormat.class);

        FileInputFormat.setInputPaths(job_conf2, new Path(args[1]));
        FileOutputFormat.setOutputPath(job_conf2, new Path(args[2]));
        my_client2.setConf(job_conf2);   
        
        // MAPREDUCER 2
        JobClient my_client3 = new JobClient();
        JobConf job_conf3 = new JobConf(MRDriver.class);
        job_conf3.setJobName("MAPREDUCER 3");
        job_conf3.setOutputKeyClass(Text.class);
        job_conf3.setOutputValueClass(CustomDataWritable.class);
        job_conf3.setMapperClass(ProductRemovedFromCartMapper3.class);
        job_conf3.setReducerClass(ProductRemovedFromCartReducer3.class);
        job_conf2.setInputFormat(TextInputFormat.class);
        job_conf3.setOutputFormat(TextOutputFormat.class);

        FileInputFormat.setInputPaths(job_conf3, new Path(args[2]));
        FileOutputFormat.setOutputPath(job_conf3, new Path(args[3]));
        my_client3.setConf(job_conf3); 
        
        try {
            JobClient.runJob(job_conf1);
            JobClient.runJob(job_conf2);
            JobClient.runJob(job_conf3);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        // System.exit(job3.waitForCompletion(true) ? 0 : 1);
    }
}

