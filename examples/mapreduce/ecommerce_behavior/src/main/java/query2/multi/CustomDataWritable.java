/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package parcialquery2mul;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.io.Writable;

public class CustomDataWritable implements Writable {

    private Text brand;
    private Text categoryCode;
    private FloatWritable price;

    public CustomDataWritable() {
        set(new Text(), new Text(), new FloatWritable());
    }

    public CustomDataWritable(String brand, String categoryCode, float price) {
        set(new Text(brand),
                new Text(categoryCode),
                new FloatWritable(price));
    }

    public CustomDataWritable(Text brand, Text categoryCode, FloatWritable price) {
        set( brand, categoryCode, price);
    }

    public void set(Text brand, Text categoryCode, FloatWritable price) {
        this.brand = brand;
        this.categoryCode = categoryCode;
        this.price = price;
    }


    public Text getBrand() {
        return brand;
    }

    public Text getCategoryCode() {
        return categoryCode;
    }

    public FloatWritable getPrice() {
        return price;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        brand.write(out);
        categoryCode.write(out);
        price.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        brand.readFields(in);
        categoryCode.readFields(in);
        price.readFields(in);
    }

    @Override
    public String toString() {
//        return "DataCustomWritable(brand=" + brand
//                + ", category_code=" + categoryCode
//                + ", price=" + price + ")";

        return brand+","+categoryCode+","+price;
    }
}
