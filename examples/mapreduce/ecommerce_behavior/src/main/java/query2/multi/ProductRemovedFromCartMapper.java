package parcialquery2mul;

import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class ProductRemovedFromCartMapper extends MapReduceBase
        implements Mapper<LongWritable, Text, Text, IntWritable> {

    private final String HEADER_EVENT_TYPE = "event_type";
    private final String targetEventType = "view";
    private final static IntWritable one = new IntWritable(1);

    @Override
    public void map(LongWritable key, Text value,
            OutputCollector<Text, IntWritable> output,
            Reporter reporter) throws IOException {

        String valueString = value.toString();
        String[] rowCSVDataset = valueString.split(",");

        // Columns
        String eventType = rowCSVDataset[1];
        String categoryCode = rowCSVDataset[4];
        String brand = rowCSVDataset[5];
        String priceText = rowCSVDataset[6];

        // Conditions
        boolean isNotEmpty = !eventType.isEmpty() && !priceText.isEmpty();
        boolean isNotFirstRow = !eventType.equals(HEADER_EVENT_TYPE);

        if (isNotEmpty && isNotFirstRow) {
            boolean isTargetEventType = eventType.equals(targetEventType);

            if (isTargetEventType) {
                String text = eventType + "," + brand + "," + categoryCode + "," + priceText;
                output.collect(
                        new Text(text),
                        one);
            }
        }
    }

}

class ProductRemovedFromCartMapper2 extends MapReduceBase
        implements Mapper<LongWritable, Text, Text, IntWritable> {

    private final String targetBrand = "baden";
    private final static IntWritable one = new IntWritable(1);

    @Override
    public void map(LongWritable key, Text value,
            OutputCollector<Text, IntWritable> output,
            Reporter reporter) throws IOException {

        String[] rowData = value.toString().split("\t");
        String[] rowValue = rowData[0].split(",");
        int valueInt = Integer.parseInt(rowData[1]);

        String eventType = rowValue[0];
        String brand = rowValue[1];
        String categoryCode = rowValue[2];
        String priceText = rowValue[3];

        boolean isNotEmpty = !brand.isEmpty();

        if (isNotEmpty) {
            boolean isTargetBrand = brand.equals(targetBrand);

            if (isTargetBrand) {
                String text = eventType + "," + brand + "," + categoryCode + "," + priceText;
                output.collect(
                        new Text(text),
                        one);
            }
        }
    }

}

class ProductRemovedFromCartMapper3 extends MapReduceBase
        implements Mapper<LongWritable, Text, Text, CustomDataWritable> {

    private final String targetCategoryCode = "shoes";

    public boolean isMatch(String regex, String text) {
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        return matcher.find();
    }

    @Override
    public void map(LongWritable key, Text value,
            OutputCollector<Text, CustomDataWritable> output,
            Reporter reporter) throws IOException {

        String[] rowData = value.toString().split("\t");
        String[] rowValue = rowData[0].split(",");
        // int valueInt = Integer.parseInt(rowData[1]);

        String eventType = rowValue[0];
        String brand = rowValue[1];
        String categoryCode = rowValue[2];
        String priceText = rowValue[3];

        boolean isNotEmpty = !categoryCode.isEmpty();

        if (isNotEmpty) {
            boolean isTargetCategoryCode = isMatch(targetCategoryCode,
                    categoryCode);

            if (isTargetCategoryCode) {

                float price = Float.parseFloat(priceText);
                output.collect(
                        new Text(eventType),
                        new CustomDataWritable(brand, categoryCode, price));
            }
        }
    }

}
