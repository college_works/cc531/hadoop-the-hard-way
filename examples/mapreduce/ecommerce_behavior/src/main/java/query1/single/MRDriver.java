package query1.single;

import query1.ProductWritable;
import query1.ProductAccumulatorWritable;

import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MRDriver {
  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();

    Job job = Job.getInstance(conf, "ProductPricesGreatestThanMean_SingleMR");
    job.setJarByClass(MRDriver.class);
    job.setMapperClass(ProductPricesGreatestThanMeanMapper.class);
    job.setCombinerClass(ProductPricesGreatestThanMeanCombiner.class);
    job.setReducerClass(ProductPricesGreatestThanMeanReducer.class);

    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(ProductAccumulatorWritable.class);
    // job.
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(ProductAccumulatorWritable.class);

    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));

    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
