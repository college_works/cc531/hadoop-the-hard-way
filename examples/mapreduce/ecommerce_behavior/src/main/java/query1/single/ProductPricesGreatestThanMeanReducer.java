package query1.single;

import query1.ProductWritable;
import query1.ProductAccumulatorWritable;

import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import java.lang.Math;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.UUID;


import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class ProductPricesGreatestThanMeanReducer extends ProductPricesGreatestThanMeanCombiner {
    private final UUID uuid = UUID.randomUUID();
    private Logger logger = Logger.getLogger(ProductPricesGreatestThanMeanReducer.class + "." + uuid.toString());


    @Override
    public void writeOutput(Text key, ProductAccumulatorWritable result, Context context)
        throws IOException, InterruptedException {

        List<ProductWritable> filteredProducts = new ArrayList<ProductWritable>();

        float meanPrices = result.getAccumulated() / result.getElements();
        for(ProductWritable product : result.getProducts()){
            if(product.getPrice() > meanPrices){
                logger.info("ReducerFilter(<mean= " + String.valueOf(meanPrices) +", value=" + product.toString() + ">)");
                filteredProducts.add(product);
            }
        }

        ProductAccumulatorWritable filtered = new ProductAccumulatorWritable();
        filtered.setAccumulated(result.getAccumulated());
        filtered.setElements(result.getElements());
        filtered.setProducts(filteredProducts);

        //logger.info("ReducerOuput(<key= " + key.toString() +", value=" + product.toString() + ">)");
        context.write(key, filtered);
    }

}
