package query1;

import java.io.IOException;

import java.io.DataOutput;
import java.io.DataInput;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.FloatWritable;


public class ProductWritable implements Writable {
    private String productId;
    private String categoryId;
    private String categoryCode;
    private String brand;
    private float price;

    public ProductWritable() {}

    public ProductWritable(String productId, String categoryId, String categoryCode, String brand, float price) {
        this.productId = productId;
        this.categoryId = categoryId;
        this.categoryCode = categoryCode;
        this.brand = brand;
        this.price = price;
    }


    public void readFields(DataInput in) throws IOException {
        productId = in.readUTF();
        categoryId = in.readUTF();
        categoryCode = in.readUTF();
        brand = in.readUTF();
        price = in.readFloat();
    }

    public void write(DataOutput out) throws IOException {
        out.writeUTF(productId);
        out.writeUTF(categoryId);
        out.writeUTF(categoryCode);
        out.writeUTF(brand);
        out.writeFloat(price);
    }

    //public void set(float value){ super.set(value); }
    public float getPrice(){ return price; }
    public String getCategoryCode(){ return categoryCode; }

    @Override
    public String toString(){
        String out = "ProductWritable(product_id=" + productId +
            ", category_id=" + categoryId + ", category_code=" + categoryCode +
            ", brand=" + brand + ", price=" + String.valueOf(price) + ")";

        return out;
    }
}
