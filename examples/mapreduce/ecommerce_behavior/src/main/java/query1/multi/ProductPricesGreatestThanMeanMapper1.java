package query1.multi;

import query1.ProductWritable;
import query1.ProductAccumulatorWritable;

import java.util.regex.Pattern;
import java.util.List;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.log4j.Logger; // logging library
import org.apache.commons.lang3.StringUtils;

//import java.time.format.DateTimeFormatter;
//import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

// csv utilities
import org.apache.commons.csv.CSVFormat; // CSV parser
import org.apache.commons.csv.CSVRecord;
import java.io.StringReader;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;


public class ProductPricesGreatestThanMeanMapper1 extends Mapper<LongWritable, Text, Text, ProductAccumulatorWritable> {
    private final static String EVENT_TYPE_CSV_HEADER = "event_type";
    private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
    private Logger logger = Logger.getLogger(ProductPricesGreatestThanMeanMapper1.class);
    private final static CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
        .setSkipHeaderRecord(true)
        .build();

    private Date START_DATE;
    private Date END_DATE;

    enum InvalidValues {
        INVALID_EVENT_TIME,
        INVALID_PRICE,
        NO_CATEGORY_PRODUCT,
        NO_PRODUCT_PRICE
    }

    public ProductPricesGreatestThanMeanMapper1(){
        try {
            START_DATE = dateFormatter.parse("2019-09-01 00:00:00 UTC");
            END_DATE = dateFormatter.parse("2019-12-01 00:00:00 UTC");
        } catch(ParseException error){
            logger.warn(error.toString());
        }
    }


    public static boolean isBetween(Date date, Date start, Date end){
        return date.after(start) && date.before(end);
    }

    public boolean isElectronic(String caterogyCode){
        Pattern pattern = Pattern.compile("electronic");
        return pattern.matcher(caterogyCode).find();
    }

    @Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        StringReader rowCSVDataset = new StringReader(value.toString());

        Iterable<CSVRecord> records = csvFormat.parse(rowCSVDataset); // parse csv row with library
        for(CSVRecord record : records){
            // check input/DataExploratory.ipynb for more details about field indexes
            String eventTimeText = record.get(0); // event_time
            String eventType = record.get(1); // event_type
            String productId = record.get(2); // product_id
            String categoryId = record.get(3); // category_id
            String categoryCode = record.get(4); // category_code
            String brand = record.get(5); // brand
            String priceText = record.get(6); // price

            try {
                if (eventType != null &&
                    !eventType.equals(EVENT_TYPE_CSV_HEADER) &&
                    !eventType.isEmpty()){

                    logger.info("eventType: " + eventType);
                    eventType = StringUtils.strip(eventType);
                    logger.info("[STRIPPED]eventType: " + eventType);

                    categoryCode = StringUtils.strip(categoryCode);
                    Date eventTime = dateFormatter.parse(eventTimeText);
                    logger.info("MapperInput([eventTime= " + eventTime.toString() +
                                ", categoryCode=" + categoryCode +
                                ", price=" + priceText + "]");

                    boolean isBetween = isBetween(eventTime, START_DATE, END_DATE);
                    logger.info("IsBetween [evenTime = " + eventTime.toString() +
                                ", START=" + START_DATE.toString() +
                                ", END=" + END_DATE.toString() +
                                ", result=" + String.valueOf(isBetween) +
                                "]");

                    // boolean isElectro = isElectronic(categoryCode);
                    // logger.info("IsElectonic[ categoryCode = " + categoryCode +
                    //             ", result=" + String.valueOf(isElectro)  + "]");

                    // validate categoryCode
                    if (isBetween(eventTime, START_DATE, END_DATE) &&
                        !categoryCode.isEmpty() &&
                        !eventType.isEmpty()){

                        float price = Float.parseFloat(priceText);
                        ProductWritable product = new ProductWritable(productId, categoryId, categoryCode, brand, price);
                        ProductAccumulatorWritable accumulator = new ProductAccumulatorWritable(product);

                        String multiKey = categoryCode + ";" + eventType;
                        logger.info("MapperOutput(<key= " + multiKey +", value=" + accumulator.toString() + ">)");
                        context.write(new Text(multiKey), accumulator);
                    }
                }
            }
            catch(ParseException error){
                logger.warn(error.toString());
                context.getCounter(InvalidValues.INVALID_EVENT_TIME).increment(1);
            }
            catch(NullPointerException error){
                logger.warn(error.toString());
                context.getCounter(InvalidValues.NO_PRODUCT_PRICE).increment(1);
            }
            catch(NumberFormatException error){
                logger.warn(error.toString());
                context.getCounter(InvalidValues.INVALID_PRICE).increment(1);
            }
        }
    }
}
