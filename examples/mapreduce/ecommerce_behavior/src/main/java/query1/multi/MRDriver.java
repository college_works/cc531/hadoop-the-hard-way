package query1.multi;

import query1.ProductWritable;
import query1.ProductAccumulatorWritable;

import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger; // logging library
import java.util.UUID;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MRDriver {
  public static void main(String[] args) throws Exception {
    Logger logger = Logger.getLogger(MRDriver.class);
    Configuration conf = new Configuration();

    // First MapReduce
    Job job1 = Job.getInstance(conf, "ProductPricesGreatestThanMean1_MultiMR");
    job1.setJarByClass(MRDriver.class);
    job1.setMapperClass(ProductPricesGreatestThanMeanMapper1.class);
    //job1.setCombinerClass(ProductPricesGreatestThanMeanReducer1.class);
    job1.setReducerClass(ProductPricesGreatestThanMeanReducer1.class);

    job1.setMapOutputKeyClass(Text.class);
    job1.setMapOutputValueClass(ProductAccumulatorWritable.class);
    // job.
    job1.setOutputKeyClass(Text.class);
    job1.setOutputValueClass(ProductAccumulatorWritable.class);


    Path tmp1 = new Path("/tmp/" + java.util.UUID.randomUUID());
    logger.info("MapReduce1 output-dir: " + tmp1.toString());

    FileInputFormat.addInputPath(job1, new Path(args[0]));
    FileOutputFormat.setOutputPath(job1, tmp1);


    // Second MapReduce

    Job job2 = Job.getInstance(conf, "ProductPricesGreatestThanMean1_MultiMR");
    job2.setJarByClass(MRDriver.class);
    job2.setMapperClass(ProductPricesGreatestThanMeanMapper2.class);
    job2.setNumReduceTasks(0);
    //job2.setCombinerClass(ProductPricesGreatestThanMeanReducer1.class);
    //job2.setReducerClass(ProductPricesGreatestThanMeanReducer1.class);

    job2.setMapOutputKeyClass(Text.class);
    job2.setMapOutputValueClass(ProductAccumulatorWritable.class);
    // job.
    job2.setOutputKeyClass(Text.class);
    job2.setOutputValueClass(ProductAccumulatorWritable.class);

    Path tmp2 = new Path("/tmp/" + java.util.UUID.randomUUID());
    logger.info("MapReduce2 output-dir: " + tmp2.toString());

    FileInputFormat.addInputPath(job2, tmp1);
    FileOutputFormat.setOutputPath(job2, tmp2);


    // // Third MapReduce

    // Job job3 = Job.getInstance(conf, "ProductPricesGreatestThanMean3_MultiMR");
    // job3.setJarByClass(MRDriver.class);
    // job3.setMapperClass(ProductPricesGreatestThanMeanMapper3.class);
    // //job2.setCombinerClass(ProductPricesGreatestThanMeanReducer1.class);
    // job3.setReducerClass(ProductPricesGreatestThanMeanReducer3.class);

    // job3.setMapOutputKeyClass(Text.class);
    // job3.setMapOutputValueClass(ProductAccumulatorWritable.class);
    // // job.
    // job3.setOutputKeyClass(Text.class);
    // job3.setOutputValueClass(ProductAccumulatorWritable.class);

    // FileInputFormat.addInputPath(job3, tmp2);
    // FileOutputFormat.setOutputPath(job3, new Path(args[1]));

    try {
        job1.waitForCompletion(true);
        job2.waitForCompletion(true);
        //job3.waitForCompletion(true);
        //job1.submit();
        //job2.submit();
        //job3.submit();
        //JobClient.runJob(job1);
        //JobClient.runJob(job2);
        //JobClient.runJob(job3);
    } catch (Exception e) {
        e.printStackTrace();
    }
  }
}
