package query1.multi;

import query1.ProductWritable;
import query1.ProductAccumulatorWritable;

import java.io.IOException;
import java.lang.InterruptedException;
import java.util.function.Predicate;

import org.apache.log4j.Logger; // logging library

import java.util.UUID;
//import java.util.HashMap;
//import java.util.Map;
//import javafx.util.Pair;
import java.util.regex.Pattern;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;


public class ProductPricesGreatestThanMeanMapper2 extends Mapper<Text, Iterable<ProductAccumulatorWritable>, Text, ProductAccumulatorWritable> {
    //private final static String EVENT_TYPE_CSV_HEADER = "event_type";
    private final UUID uuid = UUID.randomUUID();
    private Predicate<ProductWritable> isElectronicProduct = product -> isElectronic(product.getCategoryCode());
    private Logger logger = Logger.getLogger(ProductPricesGreatestThanMeanMapper2.class + "." + uuid.toString());

    public boolean isElectronic(String caterogyCode){
        Pattern pattern = Pattern.compile("electronic");
        return pattern.matcher(caterogyCode).find();
    }

    @Override
    public void map(Text key, Iterable<ProductAccumulatorWritable> values, Context context)
        throws IOException, InterruptedException {
        ProductAccumulatorWritable result = new ProductAccumulatorWritable();

        for(ProductAccumulatorWritable value : values){
            result.accumulate(value, isElectronicProduct);
        }

        writeOutput(key, result, context);
    }

    public void writeOutput(Text key, ProductAccumulatorWritable result, Context context)
        throws IOException, InterruptedException {

        logger.info("Mapper2Output(<key= " + key.toString() +", value=" + result.toString() + ">)");
        context.write(key, result);
    }
}
