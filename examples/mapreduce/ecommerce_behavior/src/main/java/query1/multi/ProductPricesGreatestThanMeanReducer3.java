package query1.multi;

import query1.ProductWritable;
import query1.ProductAccumulatorWritable;

import java.io.IOException;
import java.lang.InterruptedException;
import java.util.function.Predicate;

import org.apache.log4j.Logger; // logging library

import java.util.UUID;
//import java.util.HashMap;
//import java.util.Map;
//import javafx.util.Pair;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class ProductPricesGreatestThanMeanReducer3 extends Reducer<Text, ProductAccumulatorWritable, Text, ProductAccumulatorWritable> {
    private final UUID uuid = UUID.randomUUID();
    private Logger logger = Logger.getLogger(ProductPricesGreatestThanMeanReducer3.class + "." + uuid.toString());

    public ProductAccumulatorWritable reducer(Iterable<ProductAccumulatorWritable> values){
        UUID reducerUUID = UUID.randomUUID(); // nota parece que la clase reducer se esta reusando (ASI QUE agregaremos un identificador por reducer)
        Logger reducerLogger = Logger.getLogger(logger.getName() + "." + reducerUUID.toString());


        ProductAccumulatorWritable accumulator = new ProductAccumulatorWritable();

        for(ProductAccumulatorWritable value: values){
            accumulator.accumulate(value);
        }

        // filter products greater than mean
        float mean = accumulator.getAccumulated() / accumulator.getElements();
        Predicate<ProductWritable> greaterThanMean = product -> product.getPrice() > mean;
        ProductAccumulatorWritable filtered = new ProductAccumulatorWritable();
        filtered.accumulate(accumulator, greaterThanMean);

        return filtered;
    }

    @Override
    public void reduce(Text key, Iterable<ProductAccumulatorWritable> values, Context context)
        throws IOException, InterruptedException {
        ProductAccumulatorWritable result = reducer(values);
        writeOutput(key, result, context);
    }

    public void writeOutput(Text key, ProductAccumulatorWritable result, Context context)
        throws IOException, InterruptedException {
        logger.info("CombinerOutput(<key= " + key.toString() +", value=" + result.toString() + ">)");
        context.write(key, result);
    }
}
