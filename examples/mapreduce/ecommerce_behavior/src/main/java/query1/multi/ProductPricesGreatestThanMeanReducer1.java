package query1.multi;

import query1.ProductWritable;
import query1.ProductAccumulatorWritable;

import java.io.IOException;
import java.lang.InterruptedException;
import org.apache.log4j.Logger; // logging library

import java.util.UUID;
//import java.util.HashMap;
//import java.util.Map;
//import javafx.util.Pair;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class ProductPricesGreatestThanMeanReducer1 extends Reducer<Text, ProductAccumulatorWritable, Text, ProductAccumulatorWritable> {
    //private final static String EVENT_TYPE_CSV_HEADER = "event_type";
    private final static String EVENT_TYPE = "view";
    private final UUID uuid = UUID.randomUUID();
    private Logger logger = Logger.getLogger(ProductPricesGreatestThanMeanReducer1.class + "." + uuid.toString());

    @Override
    public void reduce(Text key, Iterable<ProductAccumulatorWritable> values, Context context)
        throws IOException, InterruptedException {
        logger.info("[Reducer1] key: " + key.toString());
        String[] keys = key.toString().split(";");

        String categoryCode = keys[0];
        String eventType = "NONE";

        if (keys.length > 1){
            eventType = keys[1];
        }

        if(eventType.equals(EVENT_TYPE)){
            writeOutput(new Text(categoryCode), values, context);
        }
        //ProductAccumulatorWritable result = reducer(values);
    }

    public void writeOutput(Text key, Iterable<ProductAccumulatorWritable> results, Context context)
        throws IOException, InterruptedException {

        for(ProductAccumulatorWritable accumulator : results){
            logger.info("Reducer1Output(<key= " + key.toString() +", value=" + accumulator.toString() + ">)");
            context.write(key, accumulator);
        }
    }
}
