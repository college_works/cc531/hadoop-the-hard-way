package query1;

import java.io.IOException;

import java.io.DataOutput;
import java.io.DataInput;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.FloatWritable;
import java.util.function.Predicate;

import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class ProductAccumulatorWritable implements Writable {
    private float accumulated;
    private int elements;
    private List<ProductWritable> products;

    public ProductAccumulatorWritable() {
        accumulated = 0.0f;
        elements = 0;
        products = new ArrayList();
    }

    public ProductAccumulatorWritable(ProductWritable product) {
        accumulated = product.getPrice();
        elements = 1;

        products = new ArrayList();
        products.add(product);
    }

    private List<ProductWritable> readProducts(DataInput in, int n)  throws IOException  {
        List<ProductWritable> deserializedProducts = new ArrayList<ProductWritable>();
        int i=0;
        while(i < n){
            ProductWritable product = new ProductWritable();
            product.readFields(in);
            deserializedProducts.add(product);
            i+=1;
        }

        return deserializedProducts;
    }

    private void writeProducts(DataOutput out)  throws IOException  {
        for(ProductWritable product: products){
            product.write(out);
        }
    }


    public void readFields(DataInput in) throws IOException {
        accumulated = in.readFloat();
        elements = in.readInt();
        products = readProducts(in, elements);
    }

    public void write(DataOutput out) throws IOException {
        out.writeFloat(accumulated);
        out.writeInt(elements);
        writeProducts(out);
    }

    public void setAccumulated(float accumulated){ this.accumulated = accumulated; }
    public void setElements(int elements){ this.elements = elements; }
    public void setProducts(List<ProductWritable> products){ this.products = products; }

    public float getAccumulated(){ return accumulated; }
    public int getElements(){ return elements; }
    public List<ProductWritable> getProducts(){ return products; }

    public void accumulate(ProductWritable product){
        products.add(product);
        accumulated += product.getPrice();
        elements += 1;
    }

    public void accumulate(ProductAccumulatorWritable accumulator){
        products.addAll(accumulator.getProducts());
        accumulated += accumulator.getAccumulated();
        elements +=  accumulator.getElements();
    }

    public void accumulate(ProductAccumulatorWritable accumulator, Predicate<ProductWritable> predicate){
        accumulated += accumulator.getAccumulated();
        elements +=  accumulator.getElements();

        products.addAll(accumulator.getProducts().stream().filter(predicate).collect(Collectors.toList()));
    }

    //@Override
    public String toString(){
        String out = "ProductAccumulatorWritable(accumulated=" +
            String.valueOf(accumulated) + ", elements=" + String.valueOf(elements) +
            ", products=" + products + ")";

        return out;
    }
}
