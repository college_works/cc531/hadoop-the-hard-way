package query9;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.FloatWritable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class EnergeticFoodReducer extends MapReduceBase
        implements Reducer<Text, FloatWritable, Text, FloatWritable> {

    public void reduce(Text t_key, Iterator<FloatWritable> values, OutputCollector<Text, FloatWritable> output, Reporter reporter) throws IOException {
        Text label = t_key;
        int frequency = 0;
        float sum = 0f;
        while (values.hasNext()) {
            // replace type of value with the actual type of our value
            FloatWritable value = values.next();
            sum += value.get(); 
            frequency += 1;
        }
        
        output.collect(label, new FloatWritable(sum / frequency));
    }
}
