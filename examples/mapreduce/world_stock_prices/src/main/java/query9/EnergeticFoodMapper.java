package query9;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.hadoop.io.FloatWritable;

public class EnergeticFoodMapper extends MapReduceBase
        implements Mapper<LongWritable, Text, Text, FloatWritable> {
    
    private final String industryTagValue = "technology";
    private final String countryValue = "usa";
    private final String OPEN_HEADER = "Open";

    public void map(LongWritable key, Text value, OutputCollector<Text, FloatWritable> output, Reporter reporter) throws IOException {

        String valueString = value.toString();
        String[] rowCSVDataset = valueString.split(",");
               
        String country = rowCSVDataset [11];
        String open = rowCSVDataset [1];
        String industryTag = rowCSVDataset [10];        
          
        boolean isFromUSA = country.equals(countryValue);
        boolean isIndustryTag = industryTag.equals(industryTagValue);
        boolean isNotFirstRow = !open.equals(OPEN_HEADER);  
        
        if (isNotFirstRow && isFromUSA && isIndustryTag) {   
            float open_ = Float.parseFloat(open);
            String label = country + " " + industryTag;
            output.collect(new Text(label), new FloatWritable(open_));
        }         
    }
}
