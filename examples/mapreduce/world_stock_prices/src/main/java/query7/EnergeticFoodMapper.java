package query7;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;


public class EnergeticFoodMapper extends MapReduceBase
        implements Mapper<LongWritable, Text, Text, OpenCloseWritable> {
    
    private final String CLOSE_HEADER = "Close";
    private final String industryTagValue = "entertainment";
    
    public void map(LongWritable key, Text value, OutputCollector<Text, OpenCloseWritable> output, Reporter reporter) throws IOException {

        String valueString = value.toString();
        String[] rowCSVDataset = valueString.split(",");
               
        String brandName = rowCSVDataset [8];
        String industryTag = rowCSVDataset [10];  
        String country = rowCSVDataset [11];
        // String date = rowCSVDataset [0];
        String open = rowCSVDataset [1];        
        String close = rowCSVDataset [4];        
          
        boolean isNotFirstRow = !close.equals(CLOSE_HEADER); 
        boolean isIndustryTag = industryTag.equals(industryTagValue);
        
        if (isNotFirstRow && isIndustryTag) {   
            float open_ = Float.parseFloat(open);
            float close_ = Float.parseFloat(close);
            String label = brandName + " " + industryTag + " " + country;
            output.collect(new Text(label), new OpenCloseWritable(open_, close_));
        }         
    }
}
