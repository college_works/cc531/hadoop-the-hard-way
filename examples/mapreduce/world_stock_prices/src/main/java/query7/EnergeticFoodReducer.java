package query7;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.FloatWritable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class EnergeticFoodReducer extends MapReduceBase
        implements Reducer<Text, OpenCloseWritable, Text, FloatWritable> {

    public void reduce(Text t_key, Iterator<OpenCloseWritable> values, OutputCollector<Text, FloatWritable> output, Reporter reporter) throws IOException {
        Text label = t_key;
        float totalOpen = 0;
        float totalClose = 0;
        
        while (values.hasNext()) {
            // replace type of value with the actual type of our value
            OpenCloseWritable value = values.next();
            FloatWritable openWritable = value.getOpen();
            FloatWritable closeWritable = value.getClose();
            
            float open = openWritable.get();
            float close = closeWritable.get();
            
            totalOpen += open;
            totalClose += close;
        }
        
        if(totalOpen != 0){
            float rate = (totalClose - totalOpen)/totalOpen;
            if (rate > 0){
              output.collect(label, new FloatWritable(rate));
            }
        }
    }
}
