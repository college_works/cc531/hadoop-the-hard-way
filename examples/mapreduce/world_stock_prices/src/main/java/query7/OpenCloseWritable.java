  /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package query7;

/**
 *
 * @author Usuario
 */
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.FloatWritable;

import org.apache.hadoop.io.Writable;

public class OpenCloseWritable implements Writable {

    private FloatWritable open;
    private FloatWritable close;

    public OpenCloseWritable() {
        set(new FloatWritable(), new FloatWritable());
    }

    public OpenCloseWritable(float open, float close) {
        set(new FloatWritable(open), new FloatWritable(close));
    }

    public OpenCloseWritable(FloatWritable open, FloatWritable close) {
        set(open, close);
    }

    public void set(FloatWritable open, FloatWritable close) {
        this.open = open;
        this.close = close;
    }

    public FloatWritable getOpen() {
        return open;
    }

    public FloatWritable getClose() {
        return close;
    }
  
    
    @Override
    public void write(DataOutput out) throws IOException {
        open.write(out);
        close.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        open.readFields(in);
        close.readFields(in);
    }

    @Override
    public String toString() {
        return "(" + open + "," + close + ")";
    }
}