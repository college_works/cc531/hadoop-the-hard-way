package query8;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import org.apache.hadoop.io.FloatWritable;

public class EnergeticFoodMapper extends MapReduceBase
        implements Mapper<LongWritable, Text, Text, FloatWritable> {
    
    private final String CLOSE_HEADER = "Close";
    
    public void map(LongWritable key, Text value, OutputCollector<Text, FloatWritable> output, Reporter reporter) throws IOException {

        String valueString = value.toString();
        String[] rowCSVDataset = valueString.split(",");
               
        String brandName = rowCSVDataset [8];
        String close = rowCSVDataset [4];
        String industryTag = rowCSVDataset [10];  
        String country = rowCSVDataset [11];   
          
        boolean isNotFirstRow = !close.equals(CLOSE_HEADER);  
        
        if (isNotFirstRow) {   
            float close_ = Float.parseFloat(close);
            String label = brandName + " " + industryTag + " " + country;
            output.collect(new Text(label), new FloatWritable(close_));
        }         
    }
}
