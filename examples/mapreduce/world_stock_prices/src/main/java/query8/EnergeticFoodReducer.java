package query8;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.FloatWritable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class EnergeticFoodReducer extends MapReduceBase
        implements Reducer<Text, FloatWritable, Text, FloatWritable> {

    public void reduce(Text t_key, Iterator<FloatWritable> values, OutputCollector<Text, FloatWritable> output, Reporter reporter) throws IOException {
        Text label = t_key;
        float minClose = Float.MAX_VALUE;
        while (values.hasNext()) {
            // replace type of value with the actual type of our value
            FloatWritable value = values.next();
            if(value.get() < minClose) minClose = value.get();
        }
        
        output.collect(label, new FloatWritable(minClose));
    }
}
