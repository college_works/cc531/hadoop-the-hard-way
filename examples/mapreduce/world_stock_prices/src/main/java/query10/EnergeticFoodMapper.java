package query10;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EnergeticFoodMapper extends MapReduceBase
        implements Mapper<LongWritable, Text, Text, IntWritable> {

    private final static IntWritable one = new IntWritable(1);

    public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {

        String valueString = value.toString();
        String[] rowCSVDataset = valueString.split(",");
        
        String industryTag = rowCSVDataset [10];
        String brandName = rowCSVDataset [8];
        String country = rowCSVDataset [11];
        
        Pattern pattern = Pattern.compile("lines", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(brandName);
        
        boolean matchFound = matcher.find();
        boolean isIndustryTagAviation = industryTag.equals("aviation");
        
        
        if (matchFound && isIndustryTagAviation) {
            output.collect(new Text(country), one);
        }         
    }
}
