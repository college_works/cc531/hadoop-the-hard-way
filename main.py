#!/usr/bin/env python3
#
# Script to provision and configure infrastructure for a production grade hadoop cluster.
#
# Hadoop Cluster Characteristics:
# - High Available (multi namenode and resource manager nodes)
# - Automatic Failover (zookeeper)
# - Security Components (kerberos)
# - Hybrid Infrastructure (Cloud providers [DYNAMIC] + On-premise)
#
#
# RESOURCES:
# - https://stackoverflow.com/questions/27590039/running-ansible-playbook-using-python-api
#
# TODO:
# - Generate dynamic inventory, from templates, when provision or deploy commands are supplied.
# - Generate inventory/group_vars variable files from templates/inventory/group_vars template files
# - [DEPRECATED] Run main.yml ansible playbook (entrypoint for playbooks on playbook/ directory)
#    - Terraform deployment take time and ansible doesn't show output of longtime-running tasks, therefore bash scripts must be called from python (Use a finite state machine to orchestate the deployment processes)
# - Use scripts/peasyshell.py script to run bash commands (e.g. terraform apply, terraform destroy, packer build, ...)
# - Define a finite state machine of deployment process (scripts/state_machine.py)
# - Save current state of finte state machine on json file (to perform operations based on current state - like destroy)
# - Move some functionalities to scripts/ directory (keep main.py script simple)
# - The initial state of the state machine when all the infrastructure is already provisioned must be "provisioned" and not "none"
# -


import argparse
from enum import Enum


#from scripts.select_unused_port import select_unused_port

class Provider(Enum):
    aws = 1
    # openstack = 2 # on progress


def provision(args: argparse.Namespace):
    """
    Provision hadoop infrastructure with supplied terraform IaC of an specific provider
    """
    pass


def configure(args: argparse.Namespace):
    """
    Configure provisioned hadoop infrastructure with ansible scripts (playbooks and roles)
    """
    pass


def deploy(args: argparse.Namespace):
    """
    Provision infrastructure with select provider and configure it.

    deploy = provision + configure
    """

    inventory = provision(args)

    # inventory: provisioned infrastructure
    # args.inventory: already provisioned infrastructure
    merged_inventory = merge_inventory(inventory, args.inventory)
    args.inventory = merged_inventory
    configuration(args)


def destroy(args: argparse.Namespace):
    pass


###### cli parser ######
parser = argparse.ArgumentParser(description='',
                                 epilog='Text at the bottom of help')

parser.add_argument('--provider-dir', default=None,
                    help="Directory to provider terraform IaC (default: provider_dir=providers/PROVIDER_NAME)")

parser.add_argument('--provider', required=True,
                    choices=[provider.name for provider in Provider],
                    help="Infrastructure provider ()")

parser.add_argument('-p', '--port',
                    help="""
                    Ouput of long-running ansible tasks will be flushed to localhost:PORT tcp socket

                    To monitor running tasks run:
                    python3 scripts/read_tcp_socket.py --port PORT

                    If no value is passed this script will find a unused port.
                    """)

parser.add_argument('-i', '--inventory', help="Inventory of hadoop cluster")
parser.add_argument('--ansible-vars', help="Variables for ansible")

subparsers = parser.add_subparsers(help='Operations')


##### deploy subparsers #####
deploy_parser = subparsers.add_parser('deploy',
                                      help="""
                                      Provision and configure a hadoop cluster
                                      """)

deploy_parser.set_defaults(func=deploy)

destroy_parser = subparsers.add_parser('destroy',
                                      help="""
                                      Destroy provisioned hadoop cluster infrastructure
                                      """)

deploy_parser.set_defaults(func=destroy)


##### provision subparsers #####
provision_parser = subparsers.add_parser('provision',
                                      help="""
                                      Provision hadoop cluster infrastructure
                                      """)

provision_parser.set_defaults(func=provision)


##### configure subparsers #####
configure_parser = subparsers.add_parser('configure',
                                      help="""
                                      Configure provisioned hadoop cluster infrastructure
                                      """)

configure_parser.set_defaults(func=configure)

if __name__=="__main__":
    args = parser.parse_args()
    args.func(args)
