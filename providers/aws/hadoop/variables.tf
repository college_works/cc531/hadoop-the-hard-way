###### Ansible variables ######
variable "ansible_verbose_level" {
  type    = number
  default = 1
}

###### General Variables ######
###### General Variables - DNS ######
variable "public_dns_zone" {
  type    = string
  default = "public_hadoopuni.com"
  description = "Public DNS for Hadoop Cluster"
}

variable "private_dns_zone" {
  type    = string
  default = "private_hadoopuni.com"
  description = "Private DNS for Hadoop Cluster"
}

# DNS nodes: ${ROLE}${COUNTER}.${var.dns_zone_name}
# EXAMPLE:
# - master namenode: namenode0000.hadoopuni.xyz
# - data namenode: datanode0123.hadoopuni.xyz

###### General Variables - EC2 Instances ######

locals {
  available_linux_flavors = ["ubuntu20.04", "ubuntu22.04", "debian11"]
  available_hadoop_versions = ["3.3.6"]

  default_hadoop_version = "3.3.6"
  default_linux_flavor   = "ubuntu22.04"
}

variable "linux_flavor" {
  type    = string
  default = "ubuntu22.04" #local.default_linux_flavor

  validation {
    condition = contains(["ubuntu20.04", "ubuntu22.04", "debian11"], var.linux_flavor) # local.available_linux_flavors
    error_message = "Valid values for linux_flavor variable: ['ubuntu20.04', 'ubuntu22.04', 'debian11']"
  }
}

variable "hadoop_version" {
  type    = string
  default = "3.3.6" #local.default_hadoop_version

  validation {
    condition = contains(["3.3.6"], var.hadoop_version) # local.available_hadoop_versions
    error_message = "Valid values for hadoop_version variable: ['3.3.6']"
  }
}


# this variable apply to any node that has no defined an specific instance type variable
# (like worker nodes, namenodes, resource amnager nodes)
variable "instance_type" {
  type    = string
  default = "t3.micro"
}


variable "key_pair" {
  type    = string
}

###### Hadoop variables ######

variable "number_worker_nodes" {
  type        = number
  default     = 3
  description = "Number of Worker (Node Manager + Data Node) nodes"
}

variable "instance_type__worker" {
  type    = string
  default = "t3.micro"
}

variable "worker_data_volume_size" {
  type    = number
  default = 10
  description = "Size of HDFS data volume"
}


###### Hadoop variables - MapReduce ######
variable "deploy_historyserver" {
  type = bool
  default = true
  description = "Deploy Job History Node for MapReduce jobs"
}

###### Hadoop variables - HDFS ######
variable "number_standby_namenode_nodes" {
  type        = number
  default     = 2
  description = "Number of Stanby NameNode nodes"
}

variable "number_observer_namenode_nodes" {
  type        = number
  default     = 2
  description = "Number of Observer NameNode nodes"
}

variable "instance_type__namenode" {
  type    = string
  default = "t3.micro"
}


###### Hadoop variables - Yarn ######
variable "number_standby_resourcemanager_nodes" {
  type        = number
  default     = 2
  description = "Number of Stanby Resource Manager nodes"
}

variable "instance_type__resourcemanager" {
  type    = string
  default = "t3.micro"
}


###### Hadoop variables - Security ######
###### Hadoop variables - High Availability ######
