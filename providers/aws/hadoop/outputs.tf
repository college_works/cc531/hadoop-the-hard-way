output "vpc_id" {
  value = module.vpc.vpc_id
}

output "public_subnets"{
  value = module.vpc.public_subnets
}

# output "namenode_dns" {
#   value = aws_route53_record.namenode.name
# }

# /*
# output "ansible_inventory" {
#   value = ansible_playbook.playbook.temp_inventory_file
# }
# */

# output "namenode_public_ip" {
#   value = aws_instance.namenode.public_ip
# }
