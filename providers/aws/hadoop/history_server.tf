########################################
# Definition of JobHistoryServer nodes
#
# NOTE: This instance is deployed only if var.deploy_historyserver == true
########################################

resource "aws_instance" "jobhistory" {
  count           = var.deploy_historyserver ? 1 : 0
  ami             = data.aws_ami.hadoop_ami
  instance_type   = var.instance_type
  security_groups = [aws_security_group.allow_all.id] # CREATE CUSTOM RULES
  # allocate the same amount of nodes in the created subnets (1 subnet by availability zone)
  subnet_id       = module.vpc.private_subnets[count.index % length(module.vpc.private_subnets)]
  key_name        = var.key_pair

  tags = {
    Name = "jobhistory"
    Role = "JobHistory"
    Zone = module.vpc.azs[count.index % length(module.vpc.private_subnets)]
  }
}
