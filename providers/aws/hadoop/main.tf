terraform {
  required_providers {
    ansible = {
      source = "ansible/ansible"
      version = "1.1.0"
    }
  }
}

data "aws_ami" "hadoop_ami" {
  filter {
    name   = "name"
    #values = ["hadoop-${var.hadoop_version}-${var.linux_flavor}-*"]
    values = ["hadoop-3.3.6-ubuntu-22.04-*"]
  }
  most_recent = true
}


# module "zookeeper" {
#   source          = "./ha/zookeeper"

#   zk_ami          = var.zookeper_ami
#   create_own_vpc  = false
#   vpc             = module.hadoop_vpc.vpc_id
#   public_subnets  = module.hadoop_vpc.public_subnets
#   private_subnets = module.hadoop_vpc.private_subnets
#   nodes           = var.zookeper_nodes
# }

# module "kerberos" {
#   source = "./security/kerberos"
# }
