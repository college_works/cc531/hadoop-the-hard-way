########################################
# Definition of resourcemanager nodes
#
# RESOURCE MANAGER NODES: active and standby
########################################

resource "aws_instance" "resourcemanager" {
  count           = sum([1, var.number_standby_resourcemanager_nodes]) # 1 primary + standby nodes
  ami             = data.aws_ami.hadoop_ami.image_id
  instance_type   = var.instance_type__resourcemanager
  security_groups = [aws_security_group.allow_resourcemanager.id] # CREATE CUSTOM RULES
  # allocate the same amount of nodes in the created subnets (1 subnet by availability zone)
  subnet_id       = module.vpc.public_subnets[count.index % length(module.vpc.public_subnets)]
  key_name        = var.key_pair
  associate_public_ip_address = true


  tags = {
    Name = "resourcemanager${count.index}"
    Role = "ResourceManager"
    Zone = module.vpc.azs[count.index % length(module.vpc.public_subnets)]
  }
}

########################################
# inventory for resourcemanager nodes
#
# NOTE: This inventory is created for post-operations of hadoop cluster resources
########################################

resource "ansible_group" "resourcemanagers" {
  name     = "resourcemanagers"
  children = []
}

resource "ansible_host" "resourcemanager" {
  count           = sum([1, var.number_standby_resourcemanager_nodes]) # 1 primary + standby nodes
  name            = aws_route53_record.resourcemanager[count.index].name
  groups          = ["resourcemanagers"]

  variables  = {
    ansible_connection = "ssh"
    ansible_user  = "ubuntu"
    ansible_ssh_private_key_file = "~/.aws/ssh/bell-virg.pem"
  }
}
