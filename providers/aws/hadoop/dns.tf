resource "aws_route53_zone" "public" {
  name = "${var.public_dns_zone}"
}

resource "aws_route53_zone" "internal" {
  name = "${var.private_dns_zone}"

  vpc {
    vpc_id = module.vpc.vpc_id
  }
}

##### Name Node DNS #####

# PUBLIC IP?
resource "aws_route53_record" "public_namenode" {
  count   = sum([1, var.number_standby_namenode_nodes]) # # 1 primary + standby nodes
  zone_id = aws_route53_zone.public.zone_id
  name    = "namenode${count.index}.${var.public_dns_zone}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.namenode[count.index].public_ip]
}

resource "aws_route53_record" "namenode" {
  count   = sum([1, var.number_standby_namenode_nodes]) # # 1 primary + standby nodes
  zone_id = aws_route53_zone.private.zone_id
  name    = "namenode${count.index}.${var.private_dns_zone}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.namenode[count.index].private_ip]
}

##### Resource Manager DNS #####


resource "aws_route53_record" "public_resourcemanager" {
  count   = sum([1, var.number_standby_resourcemanager_nodes]) # 1 primary + standby nodes
  zone_id = aws_route53_zone.public.zone_id
  name    = "resourcemanager${count.index}.${var.public_dns_zone}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.resourcemanager[count.index].public_ip]
}

resource "aws_route53_record" "resourcemanager" {
  count   = sum([1, var.number_standby_resourcemanager_nodes]) # 1 primary + standby nodes
  zone_id = aws_route53_zone.private.zone_id
  name    = "resource_manager${count.index}.${var.private_dns_zone}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.resourcemanager[count.index].private_ip]
}


##### Worker DNS #####

resource "aws_route53_record" "worker" {
  count   = var.number_worker_nodes
  zone_id = aws_route53_zone.private.zone_id
  name    = "worker${count.index}.${var.private_dns_zone}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.worker[count.index].private_ip]
}
