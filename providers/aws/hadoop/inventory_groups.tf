# Ansible groups

resource "ansible_group" "hdfs" {
  name     = "hdfs"
  children = ["namenodes", "datanodes", "journalnodes"]
}


resource "ansible_group" "yarn" {
  name     = "yarn"
  children = ["resourcemanagers", "nodemanagers"]
}


resource "ansible_group" "hadoop" {
  name     = "hadoop"
  children = ["hdfs", "yarn"]
}
