# https://stackoverflow.com/questions/62575544/create-multiple-rules-in-aws-security-group

resource "aws_security_group" "allow_all" {
  # source: https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-hdfs/HdfsUserGuide.html#Web_Interface
  name        = "allow_all"
  description = "Allow ALL inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_all"
  }
}


#### HDFS ####

##### FIREWALL #####

resource "aws_security_group" "namenode" {
  # source: https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-hdfs/HdfsUserGuide.html#Web_Interface
  name        = "allow_namenode"
  description = "Allow NameNode inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port        = 9000
    to_port          = 9000
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_namenode"
  }
}


resource "aws_security_group" "datanode" {
  name        = "allow_datanode"
  description = "Allow DataNode inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port        = 9867
    to_port          = 9867
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_datanode"
  }
}


resource "aws_security_group" "resourcemanager" {
  name        = "allow_resourcemanager"
  description = "Allow ResourceManager inbound traffic"
  vpc_id      = module.vpc.vpc_id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_datanode"
  }
}

resource "aws_security_group_rule" "resourcemanager_8030" {
  from_port        = 8030
  to_port          = 8030
  protocol         = "-1"
  cidr_blocks      = ["0.0.0.0/0"]
  ipv6_cidr_blocks = ["::/0"]
  type              = "ingress"
  security_group_id = aws_security_group.resourcemanager.id
}

resource "aws_security_group_rule" "resourcemanager_8031" {
  from_port        = 8031
  to_port          = 8031
  protocol         = "-1"
  cidr_blocks      = ["0.0.0.0/0"]
  ipv6_cidr_blocks = ["::/0"]
  type              = "ingress"
  security_group_id = aws_security_group.resourcemanager.id
}



# 8020 #
# 9870 # namenode web
# resource "aws_security_group" "namenode_sg" {
#   # source: https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-hdfs/HdfsUserGuide.html#Web_Interface
#   name        = "namenode_sg"
#   description = "Allow HTTP inbound traffic to NameNode Web Interface"
#   vpc_id      = module.vpc.vpc_id

#   egress {
#     from_port        = 0
#     to_port          = 0
#     protocol         = "-1"
#     cidr_blocks      = ["0.0.0.0/0"]
#     ipv6_cidr_blocks = ["::/0"]
#   }

#   tags = {
#     Name = "allow_all"
#   }
# }
