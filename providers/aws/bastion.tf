# bastion node to deploy and operate hadoop cluster

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}


resource "aws_instance" "bastion" {
  ami             = data.aws_ami.ubuntu.id
  instance_type   = var.instance_type__bastion
  security_groups = [aws_security_group.allow_ssh.id]
  key_name        = var.operations_key_pair
  subnet_id       = module.hadoop.public_subnets[0]
  associate_public_ip_address = true

  tags = {
    Name = "BastionNode"
  }
}


resource "aws_security_group" "allow_ssh" {
  # source: https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-hdfs/HdfsUserGuide.html#Web_Interface
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = module.hadoop.vpc_id

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}
