variable "instance_type" {
  type    = string
}

variable "region" {
  type    = string
}

variable "target" {
  type = string
}

variable "target_version" {
  type = string
}

variable "timestamp" {
  type = string
}

