#!/bin/bash
#
# Register an AMI to an ssm parameter

# if [[ $# -lt 2 ]];then
#     echo """
#     USAGE: $0 <PARAMETER_NAME> <AMI_NAME>

#     EXAMPLE:

#     $0 /hadoop/3.3.6/ubuntu-2004-20230760 hadoop-3.3.6-ubuntu2004-20230760
#     """
#     exit 1
# fi

## ENVIROMENT VARIABLES
# - PARAMETER_NAME
# - AMI_NAME

source postprocessor/resolver_ami_name.sh $AMI_NAME # return ami variable
source postprocessor/register_ssm_parameter.sh $PARAMETER_NAME $ami
