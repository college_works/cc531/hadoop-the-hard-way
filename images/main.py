#!/usr/bin/env python3
#
# Script to build custom images
#
# TODO
# - prepare build directory
#   => ansible-playbook -vvv -e="tmp_build_dir={tmp_build_dir}" --extra-vars="@vars/build.yml" prepare_build_dir.yml
# - build images of prepared build directory
#   => packer build -var-file={tmp_build_dir}/sources/REGION/variables.pkrvars.hcl {tmp_build_dir}/sources/REGION/

import os
import yaml
import argparse
import tempfile
from scripts.peasyshell import sh, init_logging
import concurrent.futures

from ansible import context
from ansible.cli import CLI
from ansible.module_utils.common.collections import ImmutableDict
from ansible.executor.playbook_executor import PlaybookExecutor
from ansible.parsing.dataloader import DataLoader
from ansible.inventory.manager import InventoryManager
from ansible.vars.manager import VariableManager


parser = argparse.ArgumentParser(description="Image builder")
parser.add_argument('--builds', type=int, default=2, help="Number of concurrent builds")
parser.add_argument('build_vars', nargs='+', help="Variables for build images")


def run_playbook(playbook, var_file, build_dir):
    loader = DataLoader()

    context.CLIARGS = ImmutableDict(tags={}, listtags=False, listtasks=False, listhosts=False,
                                syntax=False, connection='ssh',
                                module_path=None, forks=100,
                                #remote_user='vagrant', private_key_file="example.pem",
                                ssh_common_args=None, ssh_extra_args=None, sftp_extra_args=None, scp_extra_args=None,
                                become=False, become_method='sudo', become_user='root',
                                verbosity=True, check=False, start_at_task=None)

    inventory = InventoryManager(loader=loader, sources=('localhost',))

    variable_manager = VariableManager(loader=loader, inventory=inventory, version_info=CLI.version_info(gitinfo=False))
    extra_vars = loader.load_from_file(var_file)
    extra_vars['tmp_build_dir'] = build_dir
    variable_manager._extra_vars = extra_vars
    #import pdb; pdb.set_trace()

    pbex = PlaybookExecutor(playbooks=[playbook], inventory=inventory,
                        variable_manager=variable_manager, loader=loader, passwords={})

    results = pbex.run()

    return results

def packer_init(build_dir):
    # packer init {tmp_build_dir} # plugins.hcl file define required packer plugins
    sh(f"packer init {build_dir}")

def packer_build(region, build_dir):
    # packer build -var-file={tmp_build_dir}/sources/REGION/variables.pkrvars.hcl {tmp_build_dir}/sources/REGION/
    region_dir = f"sources/{region}"
    sh(f"cd {build_dir} && packer build -var-file={region_dir}/variables.pkrvars.hcl {region_dir}", shell=True)


def wait_for_future(future_builds: list):
    for future in concurrent.futures.as_completed(future_builds):
        try:
            future.result()
        except Exception as exc:
            print(f"generated an exception: {exc}")


if __name__=="__main__":
    args = parser.parse_args()

    playbook = "./prepare_build_dir.yml"
    for var_file in args.build_vars:
        with tempfile.TemporaryDirectory() as build_dir:
            run_playbook(playbook, var_file, build_dir)

            init_logging() # peasyshell logging
            packer_init(build_dir)

            regions = []
            with open(var_file, 'r') as f:
                build_vars = yaml.safe_load(f)
                regions = [source['region'] for source in build_vars['cloud_provider']['sources']]

            with concurrent.futures.ThreadPoolExecutor(max_workers=args.builds) as executor:
                future_builds = [executor.submit(packer_build, region, build_dir) for region in regions]x
                wait_for_future(future_builds)
