#!/bin/bash
#
# Hadoop client

IMAGE_NAME=hadoop-client
IMAGE_TAG=latest

DOCKERFILE=utils/client/base/Dockerfile
DOCKERFILE_DIR=$(dirname $DOCKERFILE)
docker build --name $IMAGE_NAME:$IMAGE_TAG -f $DOCKERFILE $DOCKERFILE_DIR


CONTAINER_NETWORK=bridge # container network (ensure that the container can reach namenode and resource manager nodes)

# Enviroment variables
docker run --rm -it \
       --env-file hadoop.env \
       --network $CONTAINER_NETWORK \
       -v ./examples:/examples $IMAGE_NAME:$IMAGE_TAG /bin/bash
