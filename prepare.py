#!/usr/bin/env python3

import os
import yaml
from typing import List
from jinja2 import Environment, FileSystemLoader


from ansible import context
from ansible.cli import CLI
from ansible.module_utils.common.collections import ImmutableDict
from ansible.executor.playbook_executor import PlaybookExecutor
from ansible.parsing.dataloader import DataLoader
from ansible.inventory.manager import InventoryManager
from ansible.vars.manager import VariableManager

def generate_group_vars(inventory: str, **kwargs):
    templates_dir="templates/inventory/group_vars/"
    env = Environment(
        loader = FileSystemLoader(templates_dir),
    )


    group_vars_inventory = f"{inventory}/group_vars"

    templates = env.list_templates(extensions=".j2")
    for template_name in templates:
        template = env.get_template(template_name)
        dest = os.path.join(group_vars_inventory, os.path.splitext(template_name)[0])
        dirname = os.path.dirname(dest)
        os.makedirs(dirname, exist_ok=True)

        #import pdb; pdb.set_trace()
        with open(dest, 'w') as f:
            f.write(template.render(**kwargs))

        print(f"Generated variable file: {dest}")


if __name__=="__main__":
    loader = DataLoader()

    context.CLIARGS = ImmutableDict(tags={}, listtags=False, listtasks=False, listhosts=False,
                                    syntax=False, connection='ssh',
                                    module_path=None, forks=100,
                                    remote_user='vagrant', private_key_file="example.pem",
                                    ssh_common_args=None, ssh_extra_args=None, sftp_extra_args=None, scp_extra_args=None,
                                    become=True, become_method='sudo', become_user='root',
                                    verbosity=1, diff=False, check=False, start_at_task=None)

    var_file = "./vars/test/prepare.yml"
    kwargs = {}
    with open(var_file, 'r') as f:
        kwargs = yaml.safe_load(f)

    generate_group_vars(inventory="inventory/", **kwargs)

    inventory = InventoryManager(loader=loader, sources=('inventory/',))

    variable_manager = VariableManager(loader=loader, inventory=inventory, version_info=CLI.version_info(gitinfo=False))

    pbex = PlaybookExecutor(playbooks=['./images/playbooks/hadoop.yml'], inventory=inventory,
                        variable_manager=variable_manager, loader=loader, passwords={})

    #import pdb; pdb.set_trace()
    results = pbex.run()
