#!/bin/bash
#
# Create a container with jupyter-notebook to interact hadoop cluster.


if [[ $# -lt 1 ]];then
echo "USAGE: $0 <action> [network] [enviroment] [name]

  Options:
    action: possible values: [run, build]

    Default values:
      network: $DOCKER_NETWORK
      enviroment: $ENV_FILE
      name: pyspark_RANDOM
    "
    exit 1;
fi

ACTION=$1

DOCKER_NETWORK=containers_default
ENV_FILE=hadoop.env
PORT=8888

if [[ -n $2 ]];then
  DOCKER_NETWORK=$2
fi

if [[ -n $3 ]];then
  ENV_FILE=$3
fi

CONTAINER_NAME=$4
if [[ -z "$CONTAINER_NAME" ]];then
  RANDOM=$(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom | head -c 64)
  CONTAINER_NAME="hadoop_notebook_$RANDOM"
fi

if [[ -z $IMAGE_TAG ]]; then
	IMAGE_TAG=latest
fi

# build containers/images/base/Dockerfile and utils/hadoop-client/Dockerfile images

_ACTION=$ACTION
source client.sh build
ACTION=$_ACTION

target_dir=images/notebook
docker build -t hadoop-notebook:${IMAGE_TAG} -f "$target_dir/Dockerfile" $target_dir

if [[ $ACTION == "run" ]];then
# creating container from utils/hadoop_client.dockerfile image
docker run --rm -it -p $PORT:$PORT -e NOTEBOOK_PORT=$PORT -v `pwd`/..:/hadoop --network=$DOCKER_NETWORK --env-file ${ENV_FILE} hadoop-notebook:latest
fi
