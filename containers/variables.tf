variable "instance_type" {
  type = string
  default = "t2.micro"
}


variable "extra_disk_size" {
  type  = number
  default = 50
}

variable "extra_disk_device" {
  type  = string
  default = "/dev/xvdz"
}


variable "extra_disk_type" {
  type  = string
  default = "gp3"
}
