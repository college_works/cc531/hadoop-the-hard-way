#!/bin/bash
#
# Create a container to interact with spark and hadoop cluster (yarn and hdfs).


if [[ $# -lt 1 ]];then
echo "USAGE: $0 <action> [network] [enviroment] [name]

  Options:
    action: possible values: [run, build]

    Default values:
      network: $DOCKER_NETWORK
      enviroment: $ENV_FILE
      name: pyspark_RANDOM
    "
    exit 1;
fi

ACTION=$1

DOCKER_NETWORK=containers_default
if [[ -n $2 ]];then
  DOCKER_NETWORK=$2
fi

ENV_FILE=hadoop.env
if [[ -n $3 ]];then
  ENV_FILE=$3
fi

CONTAINER_NAME=$4
if [[ -z "$CONTAINER_NAME" ]];then
  RANDOM=$(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom | head -c 64)
  CONTAINER_NAME="pyspark_$RANDOM"
fi

if [[ -z $IMAGE_TAG ]]; then
	IMAGE_TAG=latest
fi

# build containers/images/base/Dockerfile and utils/hadoop-client/Dockerfile images
_ACTION=$ACTION
source notebook.sh build
ACTION=$_ACTION

target_dir=images/spark/python
docker build -t pyspark:${IMAGE_TAG} -f "$target_dir/Dockerfile" $target_dir

if [[ $ACTION == "run" ]];then
# creating container from utils/hadoop_client.dockerfile image
docker run --rm -it  -v ./examples:/examples --network=$DOCKER_NETWORK --env-file ${ENV_FILE} pyspark:${IMAGE_TAG} /bin/bash
fi
