#!/bin/bash
#
# Add a datanode to hadoop cluster

HADOOP_NETWORK=containers_default
RANDOM=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 6 ; echo '')
CONTAINER_NAME="datanode_$RANDOM"
VOLUME_NAME="vol_$CONTAINER_NAME"


docker run -d -v $VOLUME_NAME:/hadoop/dfs/data --network=$HADOOP_NETWORK --name=$CONTAINER_NAME --env-file=hadoop.env bde2020/hadoop-datanode:2.0.0-hadoop3.2.1-java8 tail -f /dev/null
