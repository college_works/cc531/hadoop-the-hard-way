#!/bin/bash
#
# Create a container to interact with a hadoop cluster.


if [[ $# -lt 1 ]];then
echo "USAGE: $0 <action> [network] [enviroment] [name]

  Options:
    action: possible values: [run, build]

    Default values:
      network: $DOCKER_NETWORK
      enviroment: $ENV_FILE
      name: hadoop_client_RANDOM
    "
    exit 1;
fi

ACTION=$1

DOCKER_NETWORK=containers_default
if [[ -n $2 ]];then
  DOCKER_NETWORK=$2
fi

ENV_FILE=hadoop.env
if [[ -n $3 ]];then
  ENV_FILE=$3
fi

CONTAINER_NAME=$4
if [[ -z "$CONTAINER_NAME" ]];then
  RANDOM=$(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom | head -c 64)
  CONTAINER_NAME="hadoop_client_$RANDOM"
fi

if [[ -z $HADOOP_IMAGE_TAG ]]; then
	HADOOP_IMAGE_TAG=latest
fi

# build containers/images/base/Dockerfile and utils/hadoop-client/Dockerfile images
hadoop_base_dir=images/base
docker build -t hadoop-base:${HADOOP_IMAGE_TAG} -f "$hadoop_base_dir/Dockerfile" $hadoop_base_dir

hadoop_client_dir=images/client
docker build -t hadoop-client:latest -f "$hadoop_client_dir/Dockerfile" $hadoop_client_dir

if [[ $ACTION == "run" ]];then
# creating container from utils/hadoop_client.dockerfile image
docker run --rm -it  -v `pwd`/examples:/examples --network=$DOCKER_NETWORK --env-file ${ENV_FILE} hadoop-client:latest /bin/bash

#docker run --rm -it  -v ./examples:/examples --network=$DOCKER_NETWORK  hadoop-client:latest /bin/bash
fi
