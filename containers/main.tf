terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}


data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "hadoop" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  key_name      = "bell-virg"
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  associate_public_ip_address = true

  

  provisioner "local-exec" {
    command = "./wait-for ${self.public_ip}:22 -t 60; ANSIBLE_CONFIG=ansible.cfg ansible-playbook -e 'user=ubuntu' -T 60 -b -i '${self.public_ip},' main.yml"
    working_dir = path.cwd
    environment = {
      ANSIBLE_HOST_KEY_CHECKING="false"
    }
  }

  root_block_device {
    volume_size = 50
  }

  tags = {
    Name = "hadoop-instance"
  }
}

/*
resource "aws_ebs_volume" "extra" {
  availability_zone = aws_instance.hadoop.availability_zone
  size = var.extra_disk_size
  type = var.extra_disk_type
}

resource "aws_volume_attachment" "attachment" {
  volume_id   = aws_ebs_volume.extra.id
  instance_id = aws_instance.hadoop.id
  device_name = var.extra_disk_device
}
*/

resource "aws_security_group" "allow_ssh" {
  name        = "ssh-security-group"
  description = "SSH Security Group"

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Web Security Group"
  }
}


/*
resource "aws_security_group" "allow_notebook" {
  name        = "notebook-security-group"
  description = "SSH Security Group"

  ingress {
    description      = "Jupyter from VPC"
    from_port        = 8888
    to_port          = 8888
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Web Security Group"
  }
}
*/
