#!/bin/bash
#
# Script to build container images for hadoop

REPOSITORY_NAME=hadoop_the_hard_way
IMAGE_REGISTER=docker.io
IMAGE_TAG=latest

## ENVIROMENT VARIABLES
# HADOOP_IMAGE_REPOSITORY: Repository to push image (default: hadoop_the_hard_way)
# HADOOP_IMAGE_REGISTER: Image register to push image (default: docker.io)
# HADOOP_IMAGE_TAG: Tag of builded image (default: latest)
# HADOOP_JAVA_VERSION: Java version for hadoop nodes
# HADOOP_VERSION: Version of hadoop
#
##


if [[ -z ${HADOOP_IMAGE_REPOSITORY} ]];then # enviroment variable
  REPOSITORY_NAME=${HADOOP_IMAGE_REPOSITORY}
fi

if [[ -z ${HADOOP_IMAGE_REGISTER} ]]; then # enviroment variable
        IMAGE_REGISTER=${HADOOP_IMAGE_REGISTER}
endif

repository=${IMAGE_REGISTER}/${REPOSITORY_NAME}
tag="${HADOOP_IMAGE_TAG}-hadoop${HADOOP_VERSION}-java${HADOOP_JAVA_VERSION}"

# building images
docker build -t $(repository)/hadoop-base:$(tag) ./base
docker build -t $(repository)/hadoop-namenode:$(tag) ./namenode
docker build -t $(repository)/hadoop-datanode:$(tag) ./datanode
docker build -t $(repository)/hadoop-resourcemanager:$(tag) ./resourcemanager
docker build -t $(repository)/hadoop-nodemanager:$(tag) ./nodemanager
docker build -t $(repository)/hadoop-historyserver:$(tag) ./historyserver
