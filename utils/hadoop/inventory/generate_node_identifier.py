#!/usr/bin/env python3
#
# Generate namenode_id and resourcemanager_id variables for namenodes and resourcemanager (respectively)
#
# Input: inventory (directory)
# Output: populate namenode_id and resourcemanager_id on inventory/host_vars/{hostname}/main.yml

import os
import yaml
import argparse
from typing import List

from ansible.parsing.dataloader import DataLoader
from ansible.inventory.manager import InventoryManager

parser = argparse.ArgumentParser(
    description='Generate host identifier variables (namenode_id, resourcemanager_id)',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)

parser.add_argument('-i', '--inventory', required=True, help='Hadoop inventory')

parser.add_argument('--nn-prefix',
                    dest='nn_prefix',
                    default='nn',
                    help='Prefix for namenode_id variable')

parser.add_argument('--rm-prefix',
                    dest='rm_prefix',
                    default='rm',
                    help='Prefix for resourcemanager_id variable')

NUMBER_DIGITS=2

def generate_index(index: int) -> str:
    return "{:0{digits}}".format(index, digits=NUMBER_DIGITS)


def add_variable_to_yaml(filename: str, variable: str, value: str):
    """
    Add variable to yaml file or override with a new value
    """
    variables = {}
    if os.path.isfile(filename):
        with open(filename, 'r') as f:
            variables = yaml.safe_load(f)


    print(f"Adding variable: {variable}, value: {value} to {filename}")
    variables[variable] = value
    with open(filename, 'w') as f:
        yaml.dump(variables, f)

def generate_identifiers(host_vars: str, *,
                         variable: str,
                         prefix: str,
                         nodes: List[str]):
    for index, node in enumerate(nodes, start=1):
        filename = os.path.join(host_vars, f"{node}.yml")
        value = f"{prefix}{generate_index(index)}"
        add_variable_to_yaml(filename, variable, value)

def sort_nodes_by_name(nodes):
    return sorted(nodes, key=lambda x: x.get_name())


if __name__=="__main__":
    args = parser.parse_args()

    loader = DataLoader()
    inventory = InventoryManager(loader=loader, sources=(args.inventory,))


    host_vars = os.path.join(args.inventory, "host_vars")
    os.makedirs(host_vars, exist_ok=True)

    ##### namenodes #####
    primary_namenode = [inventory.get_hosts(pattern='primary_namenode')[0].get_name()]
    standby_namenodes = inventory.get_hosts(pattern='standby_namenodes')
    observer_namenodes = inventory.get_hosts(pattern='observer_namenodes')

    nodes = primary_namenode +\
        sort_nodes_by_name(standby_namenodes) +\
        sort_nodes_by_name(observer_namenodes)

    #import pdb; pdb.set_trace()
    generate_identifiers(host_vars,
                         variable="namenode_id",
                         prefix=args.nn_prefix,
                         nodes=nodes)


    ##### resourcemanagers #####
    resourcemanagers = sort_nodes_by_name(inventory.get_hosts(pattern='resourcemanagers'))
    generate_identifiers(host_vars,
                         variable="resourcemanager_id",
                         prefix=args.rm_prefix,
                         nodes=resourcemanagers)
